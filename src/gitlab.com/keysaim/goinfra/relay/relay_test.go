package relay

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func init() {
	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")
}

func TestUdp(t *testing.T) {
	param := &RelayParam{
		Mode:    "udp",
		LclPort: "19999",
		RmtIp:   "127.0.0.1",
		RmtPort: "20000",
	}
	pctx := context.Background()
	urp := NewRelayPort(pctx, param)
	err := urp.Start()
	require.Nil(t, err)
	time.Sleep(time.Duration(2) * time.Second)
	require.True(t, urp.IsReady())
	urp.Write([]byte("hello world"))
	time.Sleep(time.Duration(2000) * time.Second)
	//urp.Stop()
	err = <-urp.Done()
	require.Nil(t, err)
}
