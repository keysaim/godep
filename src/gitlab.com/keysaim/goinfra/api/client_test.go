package api

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	qpdb "gitlab.com/q-park/qpdb/db"
)

var host string = "127.0.0.1"
var port int = 8000

func TestClient(t *testing.T) {
	c := NewClient(host, port)

	mds, err := c.Find("version", nil)
	version := (*mds.(*[]qpdb.VersionModel))[0]

	mds, err = c.Find("lockgroup", nil)
	group := (*mds.(*[]qpdb.LockGroupModel))[0]

	imei := "api-client"
	mds, err = c.Find("lock", map[string]string{"imei": imei})
	pls := *mds.(*[]qpdb.LockModel)

	var lock *qpdb.LockModel
	if len(pls) > 0 {
		lock = &pls[0]
	} else {
		lock = &qpdb.LockModel{
			Imei:    "api-client",
			Version: version.Code,
			Group:   group.Name,
		}
		md, err := c.Post("lock", lock)
		require.Nil(t, err)
		lock = md.(*qpdb.LockModel)
	}

	ls := &qpdb.LockStatusModel{
		Lock:   lock.OpenId,
		Status: qpdb.LSTATUS_TOP,
		Power:  95,
	}

	mds, err = c.Find("lockstatus", map[string]string{"lock": ls.Lock})
	require.Nil(t, err)
	plss, ok := mds.(*[]qpdb.LockStatusModel)
	require.True(t, ok)
	lss := *plss
	if len(lss) > 0 {
		err = c.Del("lockstatus", lss[0].Id)
		require.Nil(t, err)
	}

	md, err := c.Post("lockstatus", ls)
	require.Nil(t, err)
	nls := md.(*qpdb.LockStatusModel)

	mds, err = c.Find("lockstatus", nil)
	require.Nil(t, err)
	plss, ok = mds.(*[]qpdb.LockStatusModel)
	require.True(t, ok, fmt.Sprintf("Invalid model type: %T", mds))
	require.NotNil(t, plss)
	lss = *plss
	require.True(t, len(lss) > 0)

	mds, err = c.Find("lockstatus", map[string]string{"lock": ls.Lock})
	require.Nil(t, err)

	md, err = c.Get("lockstatus", nls.Id)
	require.Nil(t, err)
	require.NotNil(t, md)

	ls.Power = 100
	err = c.Put("lockstatus", nls.Id, ls)
	require.Nil(t, err)

	md, err = c.Get("lockstatus", nls.Id)
	require.Nil(t, err)
	nls = md.(*qpdb.LockStatusModel)
	require.Equal(t, ls.Power, nls.Power)

	err = c.Del("lockstatus", nls.Id)
	require.Nil(t, err)
}
