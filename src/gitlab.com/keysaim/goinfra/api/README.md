# About the API style

We mainly make the API style based on:
* [JSON Hijacking](https://haacked.com/archive/2009/06/25/json-hijacking.aspx/)

    From this, we always use top level object for JSON array in response.

* [10 Best Practices for Better RESTful API](https://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/)

    Refer to this, our API path style is:
    |-----------+------------------------+--------------------------+------------------------+------------------------|
    | resource  | GET                    | POST                     | PUT                    | DELETE                 |
    |-----------+------------------------+--------------------------+------------------------+------------------------|
    | /cars     | Returns a list of cars | Create a new car         | Bulk update of cars    | Delete all cars        |
    | /cars/110 | Returns a specific car | Method not allowed (405) | Updates a specific car | Deletes a specific car |
    |-----------+------------------------+--------------------------+------------------------+------------------------|

    To create a list of cars, use `POST /cars/batch`

    And for relationship, it is:

    |---------------------+---------------------------------------|
    | resource            | GET                                   |
    |---------------------+---------------------------------------|
    | /cars/711/drivers/  | Returns a list of drivers for car 711 |
    | /cars/711/drivers/4 | Returns driver #4 for car 711         |
    |---------------------+---------------------------------------|

    And the path will always have the version.

