package api

import (
	"errors"
	"fmt"
	neturl "net/url"

	"github.com/go-resty/resty"
	qpdb "gitlab.com/keysaim/goinfra/db"
	"gitlab.com/keysaim/goinfra/log"
)

type ApiClient struct {
	version string
	Host    string
	Port    int
	url     string
}

func NewClient(host string, port int) *ApiClient {

	c := &ApiClient{
		Host: host,
		Port: port,
	}
	c.SetVersion("1.0")
	return c
}

func (c *ApiClient) SetVersion(version string) {
	c.version = version
	c.url = fmt.Sprintf("http://%s:%d/api/%s/", c.Host, c.Port, version)
}

func (c *ApiClient) Url(name string, id int64, params map[string]string) string {

	resName := GetResName(name)
	url := c.url + resName
	if id >= 0 {
		idstr := fmt.Sprintf("%d", id)
		url += "/" + idstr
		return url
	}

	if len(params) == 0 {
		return url
	}

	query := neturl.Values{}
	for k, v := range params {
		query.Set(k, v)
	}
	url += "?" + query.Encode()
	return url
}

func (c *ApiClient) Post(name string, data interface{}) (interface{}, error) {

	url := c.Url(name, -1, nil)
	md := qpdb.NewModel(name)
	if md == nil {
		return nil, errors.New("Invalid api name: " + name)
	}
	return c.Request(md, "POST", url, data)
}

func (c *ApiClient) Get(name string, id int64) (interface{}, error) {

	url := c.Url(name, id, nil)
	md := qpdb.NewModel(name)
	if md == nil {
		return nil, errors.New("Invalid api name: " + name)
	}
	return c.Request(md, "GET", url, nil)
}

func (c *ApiClient) Find(name string, params map[string]string) (interface{}, error) {

	url := c.Url(name, -1, params)
	mds := qpdb.NewModels(name, 0)
	if mds == nil {
		return nil, errors.New("Invalid api name: " + name)
	}
	ret := &TopObj{mds}
	res, err := c.Request(ret, "GET", url, nil)
	nret, ok := res.(*TopObj)
	if !ok {
		return nil, errors.New(fmt.Sprintf("Invalid result type: %T", res))
	}
	if err == nil {
		return nret.Objs, nil
	}
	return nil, err
}

func (c *ApiClient) Put(name string, id int64, data interface{}) error {
	url := c.Url(name, id, nil)
	_, err := c.Request(nil, "PUT", url, data)
	return err
}

func (c *ApiClient) Del(name string, id int64) error {
	url := c.Url(name, id, nil)
	_, err := c.Request(nil, "DELETE", url, nil)
	return err
}

func (c *ApiClient) Request(result interface{}, method, url string, data interface{}) (interface{}, error) {

	r := resty.R()
	if result != nil {
		r.SetResult(result)
	}
	if data != nil {
		r.SetBody(data)
	}
	log.Infof("Send api request: %s %s", method, url)
	rsp, err := r.Execute(method, url)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	defer rsp.RawBody().Close()

	if rsp.StatusCode() != 200 {
		return nil, errors.New("The response is not 200 OK: " + rsp.String())
	}

	return rsp.Result(), nil
}
