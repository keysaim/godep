package api

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/keysaim/goinfra/db"
	"gitlab.com/keysaim/goinfra/log"
)

type ModelHandler struct {
	*BaseHandler
	Dh           db.Handler
	name         string
	resName      string
	HookOnBinded func(c echo.Context, method string, data interface{}) error
	HookOnAdded  func(c echo.Context, method string, data interface{}) error
}

func NewModelHandler(dbCtx db.Context, name string) *ModelHandler {

	h := &ModelHandler{
		BaseHandler: NewBaseHandler(dbCtx),
		name:        name,
		resName:     GetResName(name),
	}

	dh := dbCtx.GetHandler(name)
	if dh == nil {
		log.Fatal("Failed to get the db handler")
	}
	h.Dh = dh
	return h
}

func (h *ModelHandler) ResName() string {
	return h.resName
}

func (h *ModelHandler) Post(c echo.Context) (err error) {

	m := h.Dh.New()
	if err = c.Bind(m); err != nil {
		return err
	}
	if h.HookOnBinded != nil {
		err = h.HookOnBinded(c, "post", m)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	log.Debugf("POST model: %#v", m)
	err = h.Dh.Add(m)
	if err != nil {
		return err
	}
	if h.HookOnAdded != nil {
		err = h.HookOnAdded(c, "post", m)
		if err != nil {
			log.Error(err)
			h.Dh.Del(m.ID())
			return err
		}
	}
	return c.JSON(http.StatusOK, m)
}

func (h *ModelHandler) Mpost(c echo.Context) (err error) {

	ml := h.Dh.Mnew(0)
	if err = c.Bind(ml); err != nil {
		log.Error(err)
		return err
	}
	if h.HookOnBinded != nil {
		err = h.HookOnBinded(c, "mpost", ml)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	log.Debugf("POST models: %#v", ml)
	err = h.Dh.Madd(ml)
	if err != nil {
		return err
	}
	return c.String(http.StatusOK, "")
}

func (h *ModelHandler) Get(c echo.Context) error {

	id, err := h.ParamInt64(c, "id")
	if err != nil {
		return err
	}

	m, err := h.Dh.Get(id)
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	if m == nil {
		return h.Error(http.StatusNotFound, "No "+h.name+" found with id: "+strconv.FormatInt(id, 10))
	}

	return c.JSON(http.StatusOK, m)
}

func (h *ModelHandler) Find(c echo.Context) (err error) {

	sp := h.QparamsToDb(c)
	var ml interface{}
	if len(sp.Conds) == 0 {
		ml, err = h.Dh.Find(sp.Where, sp.Order, sp.Offset, sp.Limit)
	} else {
		ml, err = h.Dh.Join(sp)
	}
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound)
	}

	o := NewTopObj(ml)

	return c.JSON(http.StatusOK, o)
}

func (h *ModelHandler) Count(c echo.Context) error {

	sp := h.QparamsToDb(c)
	numStr, err := h.Dh.Func(sp.Where, sp.Order, sp.Offset, sp.Limit, []string{"COUNT", "*"})
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound)
	}

	num, err := strconv.Atoi(numStr)
	if err != nil {
		log.Error(err)
		return err
	}
	var rsp = map[string]int{
		"count": num,
	}

	return c.JSON(http.StatusOK, rsp)
}

func (h *ModelHandler) Put(c echo.Context) error {

	id, err := h.ParamInt64(c, "id")
	if err != nil {
		return err
	}

	m := h.Dh.New()
	if err = c.Bind(m); err != nil {
		return err
	}
	if h.HookOnBinded != nil {
		err = h.HookOnBinded(c, "put", m)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	log.Debugf("PUT model: %#v", m)
	err = h.Dh.Put(id, m)
	if err != nil {
		return err
	}
	return echo.NewHTTPError(http.StatusOK)
}

func (h *ModelHandler) Mput(c echo.Context) (err error) {

	ml := h.Dh.Mnew(0)
	if err = c.Bind(ml); err != nil {
		return err
	}
	if h.HookOnBinded != nil {
		err = h.HookOnBinded(c, "mput", ml)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	log.Debugf("PUT models: %#v", ml)
	err = h.Dh.Mput(ml)
	if err != nil {
		return err
	}
	return c.String(http.StatusOK, "")
}

func (h *ModelHandler) Delete(c echo.Context) error {

	id, err := h.ParamInt64(c, "id")
	if err != nil {
		return err
	}

	log.Debugf("DELETE %s model with ID: %d", h.name, id)
	err = h.Dh.Del(id)
	if err != nil {
		return err
	}
	return c.String(http.StatusOK, "")
}

func (h *ModelHandler) Spec(c echo.Context) error {

	var spec = map[string]map[string][]string{
		"fields": map[string][]string{
			"POST": db.TableFields(h.name, db.DB_OP_ADD),
			"PUT":  db.TableFields(h.name, db.DB_OP_PUT),
		},
	}

	return c.JSON(http.StatusOK, &spec)
}
