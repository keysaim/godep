package api

import (
	"errors"
	"strconv"
	"strings"

	"github.com/jinzhu/inflection"
	"github.com/labstack/echo"
	"gitlab.com/keysaim/goinfra/db"
	"gitlab.com/keysaim/goinfra/log"
)

var ErrNoSuchParam = errors.New("No such param name")
var resNameMap map[string]string = map[string]string{}

func GetResName(name string) string {

	if rn, ok := resNameMap[name]; !ok {
		rn = inflection.Plural(name)
		resNameMap[name] = rn
		return rn
	} else {
		return rn
	}
}

type Handler interface {
	ResName() string
	Post(c echo.Context) error
	Mpost(c echo.Context) error
	Get(c echo.Context) error
	Find(c echo.Context) error
	Count(c echo.Context) error
	Put(c echo.Context) error
	Mput(c echo.Context) error
	Delete(c echo.Context) error
	Spec(c echo.Context) error // Return the api spec
}

type TopObj struct {
	Objs interface{}
}

func NewTopObj(slice interface{}) *TopObj {

	return &TopObj{
		Objs: slice,
	}
}

type BaseHandler struct {
	DbCtx db.Context
}

func NewBaseHandler(dbCtx db.Context) *BaseHandler {

	return &BaseHandler{
		DbCtx: dbCtx,
	}
}

func (h *BaseHandler) Error(status int, msg string) error {
	return echo.NewHTTPError(status, msg)
}

func (h *BaseHandler) Post(c echo.Context) error {
	log.Warnf("Post method not implement in Handler %T", h)
	return db.ErrNotImplement
}

func (h *BaseHandler) Get(c echo.Context) error {
	log.Warnf("Get method not implement in Handler %T", h)
	return db.ErrNotImplement
}

func (h *BaseHandler) Put(c echo.Context) error {
	log.Warnf("Put method not implement in Handler %T", h)
	return db.ErrNotImplement
}

func (h *BaseHandler) Delete(c echo.Context) error {
	log.Warnf("Delete method not implement in Handler %T", h)
	return db.ErrNotImplement
}

func (h *BaseHandler) ParamInt64(c echo.Context, name string) (int64, error) {

	s := c.Param(name)
	if s == "" {
		log.Errorf("No such name %s in path params", name)
		return 0, ErrNoSuchParam
	}
	v, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, err
	}
	return v, nil
}

func (h *BaseHandler) ParamInt(c echo.Context, name string) (int, error) {

	if v, err := h.ParamInt64(c, name); err != nil {
		return 0, err
	} else {
		return int(v), nil
	}
}

func (h *BaseHandler) QparamInt(c echo.Context, name string) (int, error) {

	s := c.QueryParam(name)
	if s == "" {
		return 0, ErrNoSuchParam
	}

	v, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0, err
	}
	return int(v), nil
}

func (h *BaseHandler) QparamInt64(c echo.Context, name string) (int64, error) {

	s := c.QueryParam(name)
	if s == "" {
		return 0, ErrNoSuchParam
	}

	v, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, err
	}
	return v, nil
}

func (h *BaseHandler) QparamPopOffset(c echo.Context) int {

	v, err := h.QparamInt(c, "offset")
	delete(c.QueryParams(), "offset")
	if err != nil {
		return 0
	}
	return v
}

func (h *BaseHandler) QparamPopLimit(c echo.Context) int {

	v, err := h.QparamInt(c, "limit")
	delete(c.QueryParams(), "limit")
	if err != nil {
		return 0
	}
	return v
}

func (h *BaseHandler) SetJoinParams(c echo.Context, p *db.JoinParams) {
	c.Set("join", p)
}

func (h *BaseHandler) QparamPopJoin(c echo.Context) *db.JoinParams {

	s := c.QueryParam("join")
	delete(c.QueryParams(), "join")
	v := c.Get("join")
	if v != nil {
		p, ok := v.(*db.JoinParams)
		if !ok {
			log.Errorf("The set join value is not *JoinParams type, but: %T", v)
			return nil
		}
		return p
	}
	if s == "" {
		return nil
	}

	segs := strings.Split(s, ",")
	if len(segs) < 2 {
		log.Errorf("The join query must have at least two segs split by ',', but now it's: %s", s)
		return nil
	}
	p := &db.JoinParams{}
	ssegs := strings.Split(segs[0], ":")
	t := db.INNER_JOIN
	if len(ssegs) > 1 {
		switch ssegs[0] {
		case "left":
			t = db.LEFT_JOIN
		case "dleft":
			t = db.LEFT_DIFF_JOIN
		default:
			log.Errorf("Unsupported join type: %s", ssegs[0])
			return nil
		}
		p.Rname = ssegs[1]
	} else {
		p.Rname = ssegs[0]
	}
	p.Type = t
	conds := make(map[string]string)
	for i := 1; i < len(segs); i++ {
		ssegs = strings.Split(segs[i], ":")
		if len(ssegs) < 2 {
			log.Errorf("Invalid join condition: %s", segs[i])
			return nil
		}
		conds[ssegs[0]] = ssegs[1]
	}
	p.Conds = conds
	return p
}

func (h *BaseHandler) QparamsToDb(c echo.Context) *db.SqlParams {

	sp := db.NewSqlParams()
	jparams := h.QparamPopJoin(c)
	if jparams != nil {
		sp.Jtype = jparams.Type
		sp.Rname = jparams.Rname
		sp.Conds = jparams.Conds
	}
	sp.Offset = h.QparamPopOffset(c)
	sp.Limit = h.QparamPopLimit(c)
	qparams := c.QueryParams()

	ostr := qparams.Get("orderby")
	delete(qparams, "orderby")
	if ostr != "" {
		sp.Order = strings.Split(ostr, ",")
	}

	if len(qparams) > 0 {
		where := make(map[string]string)
		for k, vl := range qparams {
			where[k] = vl[0]
		}
		sp.Where = where
	}

	return sp
}
