package monitor

import (
	"../iox/buffer"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/netx/tcp"
	"../protocol/gt03"
)

type Session struct {
	conn     *tcp.TcpConn
	msgChan  chan<- *gt03.Gt03Pkt
	buf      *buffer.ValueBuf
	exitChan chan error
}

func NewSession(conn *tcp.TcpConn) *Session {
	return &Session{
		conn:     conn,
		buf:      buffer.NewValueBuf(1024),
		exitChan: make(chan error),
	}
}

func (s *Session) Start(msgChan chan<- *gt03.Gt03Pkt, exitChan chan<- error) {
	s.msgChan = msgChan
	s.conn.Start(s.onRead, s.exitChan)
	go s.waitExit(exitChan)
}

func (s *Session) Stop() {
	s.conn.Stop()
}

func (s *Session) onRead(data []byte) error {
	log.Infof("Session %p received data: %X", s, data)
	_, err := s.buf.Write(data)
	if err != nil {
		if err != gt03.ERR_PKT_INCOMPLETE {
			log.Error("Write data failed, will teardown the connection,", err)
			return err
		}
		return nil
	}

	pkt := gt03.NewPackage()
	_, err = pkt.Decode(s.buf)
	if err != nil {
		if err != gt03.ERR_PKT_INCOMPLETE {
			log.Error("Parse gt03 protocol failed, will reset the buffer,", err)
			s.buf.Reset()
		}
		return nil
	}

	s.buf.ShrinkKeep()
	s.msgChan <- pkt
	return nil
}

func (s *Session) Reply(pkt *gt03.Gt03Pkt) error {
	buf := buffer.NewValueBuf(1024)
	_, err := pkt.EncodeRsps(buf)
	if err != nil {
		log.Error("Decode gt03 package failed:", err)
		return err
	}

	s.conn.Send(buf.Bytes())
	return nil
}

func (s *Session) ReplyError(pkt *gt03.Gt03Pkt) error {
	//fake := [...]byte{0x00, 0x00}
	//s.conn.Send(fake[:])
	//return nil
	return s.Reply(pkt)
}

func (s *Session) waitExit(exitChan chan<- error) {
	err := <-s.exitChan
	log.Info("Session to exit with err:", err)
	exitChan <- err
}
