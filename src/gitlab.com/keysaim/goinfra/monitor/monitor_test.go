package monitor

import (
	"fmt"
	"net"
	"testing"
	"time"

	"database/sql"
	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type MonitorTest struct {
	suite.Suite
	config map[string]string
}

func (ts *MonitorTest) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")

	config := map[string]string{
		"mysql_user":     "root",
		"mysql_password": "default",
		"mysql_host":     "localhost",
		"mysql_port":     "3306",
		"mysql_db":       "test_db",
		"port":           "33333",
		"sampleUrl":      "http://182.92.158.18:9999/pos/post/realtimeinfo/",
		//"sampleUrl":      "http://demo.rush-tech.com:9999/pos/post/realtimeinfo/",
	}
	ts.config = config

	ts.prepareDb()
}

func (ts *MonitorTest) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *MonitorTest) prepareDb() {
	conf := ts.config
	authStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/", conf["mysql_user"], conf["mysql_password"],
		conf["mysql_host"], conf["mysql_port"])
	db, err := sql.Open("mysql", authStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	_, err = db.Exec("DROP DATABASE " + conf["mysql_db"])
	log.Errorf("Drop DB with err: %s", err)

	_, err = db.Exec("CREATE DATABASE " + conf["mysql_db"])
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("USE " + conf["mysql_db"])
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("CREATE TABLE client_info (car_id int,serial varchar(64))")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("INSERT INTO client_info (car_id,serial) VALUES (100,\"868120143626205\")")
	if err != nil {
		panic(err)
	}
}

func (ts *MonitorTest) TestStartStop() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	ts.config["port"] = "55555"
	monitor := NewMonitor(ts.config)
	err := monitor.Start()
	assert.Nil(err)
	monitor.Stop()
}

func (ts *MonitorTest) TestConnect() {
	assert := require.New(ts.T())

	ts.config["port"] = "33333"
	monitor := NewMonitor(ts.config)
	err := monitor.Start()
	assert.Nil(err)
	manager := monitor.SessionManager()

	addr := fmt.Sprintf("127.0.0.1:%s", ts.config["port"])
	log.Infof("listen addr: %s", addr)
	count := 10
	for idx := 0; idx < count; idx += 1 {
		conn, err := net.Dial("tcp", addr)
		if err != nil {
			log.Errorf("Failed to dial server with addr: %s, err: %s", addr, err)
		}
		assert.Nil(err)
		assert.NotNil(conn)
	}

	time.Sleep(2 * time.Second)
	assert.Equal(count, manager.SessionCount())
	monitor.Stop()
	assert.Equal(0, manager.SessionCount())
}

func (ts *MonitorTest) TestLogin() {
	assert := require.New(ts.T())

	ts.config["port"] = "11111"
	monitor := NewMonitor(ts.config)
	err := monitor.Start()
	assert.Nil(err)
	manager := monitor.SessionManager()

	addr := fmt.Sprintf("127.0.0.1:%s", ts.config["port"])
	log.Infof("listen addr: %s", addr)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		log.Errorf("Failed to dial server with addr: %s, err: %s", addr, err)
	}
	assert.Nil(err)
	assert.NotNil(conn)

	// Login info
	raw := [...]byte{0x78, 0x78, 0x11, 0x01, 0x08, 0x68, 0x12, 0x01, 0x43, 0x62, 0x62, 0x05, 0x20, 0x20, 0x32, 0x02, 0x00, 0x7a, 0xe0, 0x7c, 0x0d, 0x0a}
	n, err := conn.Write(raw[:])
	assert.Equal(len(raw), n)
	assert.Nil(err)

	buf := make([]byte, 1024)
	conn.SetReadDeadline(time.Now().Add(2 * time.Second))
	size, err := conn.Read(buf)
	assert.Nil(err)

	rsps := [...]byte{0x78, 0x78, 0xf, 0x1, 0x8, 0x68, 0x12, 0x1, 0x43, 0x62, 0x62, 0x5, 0x20, 0x20, 0x0, 0x7a, 0xe8, 0x12, 0xd, 0xa}
	assert.Equal(rsps[:], buf[:size])

	// GPS info
	gps_raw := [...]byte{0x78, 0x78, 0x19, 0x10, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x9C, 0x02, 0x7A, 0xC7, 0xEB, 0x0C, 0x46, 0x58, 0x49, 0x00, 0x14, 0x8F,
		0x00, 0x01, 0x00, 0x03, 0x80, 0x81, 0x0D, 0x0A}

	n, err = conn.Write(gps_raw[:])
	assert.Equal(len(gps_raw), n)
	assert.Nil(err)

	buf = make([]byte, 1024)
	conn.SetReadDeadline(time.Now().Add(5 * time.Second))
	size, err = conn.Read(buf)
	assert.Nil(err)

	gps_rsps := [...]byte{0x78, 0x78, 0x05, 0x10, 0x00, 0x03, 0x25, 0x87, 0x0D, 0x0A}
	assert.Equal(gps_rsps[:], buf[:size])

	monitor.Stop()
	assert.Equal(0, manager.SessionCount())
}

func (ts *MonitorTest) TestInvalidLogin() {
	assert := require.New(ts.T())

	ts.config["port"] = "11112"
	monitor := NewMonitor(ts.config)
	err := monitor.Start()
	assert.Nil(err)
	manager := monitor.SessionManager()

	addr := fmt.Sprintf("127.0.0.1:%s", ts.config["port"])
	log.Infof("listen addr: %s", addr)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		log.Errorf("Failed to dial server with addr: %s, err: %s", addr, err)
	}
	assert.Nil(err)
	assert.NotNil(conn)

	raw := [...]byte{0x78, 0x78, 0x11, 0xe1, 0x08, 0x68, 0x12, 0x01, 0x43, 0x62, 0x62, 0x05, 0x20, 0x20, 0x32, 0x02, 0x00, 0x7a, 0xe0, 0x7c, 0x0d, 0x0a}
	n, err := conn.Write(raw[:])
	assert.Equal(len(raw), n)
	assert.Nil(err)

	buf := make([]byte, 1024)
	conn.SetReadDeadline(time.Now().Add(2 * time.Second))
	_, err = conn.Read(buf)
	assert.NotNil(err)

	raw = [...]byte{0x78, 0x78, 0x11, 0x01, 0x08, 0x69, 0x12, 0x01, 0x43, 0x62, 0x62, 0x05, 0x20, 0x20, 0x32, 0x02, 0x00, 0x7a, 0xe0, 0x7c, 0x0d, 0x0a}
	n, err = conn.Write(raw[:])
	assert.Equal(len(raw), n)
	assert.Nil(err)

	buf = make([]byte, 1024)
	conn.SetReadDeadline(time.Now().Add(2 * time.Second))
	_, err = conn.Read(buf)
	assert.Nil(err)

	monitor.Stop()
	assert.Equal(0, manager.SessionCount())
}

func TestTcpServer(t *testing.T) {
	suite.Run(t, new(MonitorTest))
}
