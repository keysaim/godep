package monitor

import (
	"strconv"
	"strings"

	"gitlab.com/keysaim/goinfra/log"
)

type RtInfo struct {
	Series   string  `json:"series"`
	CarId    int     `json:"carId"`
	UsageId  int     `json:"usageId"`
	Oil      float64 `json:"oil"`
	Dis      float64 `json:"dis"`
	Realtime string  `json:"realTime"`
	Lat      float64 `json:"lat"`
	Lng      float64 `json:"lng"`
	Speed    int     `json:"speed"`
	Client   string  `json:"client"`
	Power    int     `json:"power"`
}

type StatusInfo struct {
	Series string `json:"series"`
	Power  int    `json:"power"`
}

type RtResp struct {
	Code    string `json:"code"`
	CarId   string `json:"carId"`
	UsageId string `json:"usageId"`
}

func (rp *RtResp) GetCarId() int {
	cstr := strings.Trim(rp.CarId, "0")
	i, err := strconv.Atoi(cstr)
	if err != nil {
		log.Errorf("Invalid CarId string: %s", cstr)
		return -1
	}

	return i
}

func (rp *RtResp) GetUsageId() int {
	ustr := strings.Trim(rp.UsageId, "0")
	i, err := strconv.Atoi(ustr)
	if err != nil {
		log.Errorf("Invalid UsageId string: %s", ustr)
		return -1
	}

	return i
}
