package main

import (
	"flag"
	"fmt"

	"./log"
	"./monitor"
)

func main() {
	pDbName := flag.String("db-name", "rushgps", "MySql DB name")
	pPort := flag.String("port", "9999", "Listen port")
	pPostAddr := flag.String("post-addr", "127.0.0.1:9999", "Post address, ip:port")
	flag.Parse()

	sampleUrl := fmt.Sprintf("http://%s/pos/post/realtimeinfo/", *pPostAddr)

	config := map[string]string{
		"mysql_user":     "root",
		"mysql_password": "default",
		"mysql_host":     "localhost",
		"mysql_port":     "3306",
		"mysql_db":       *pDbName,
		"port":           *pPort,
		"sampleUrl":      sampleUrl,
	}

	fmt.Printf("config: %#v\n", config)

	exitChan := make(chan error)
	monitor := monitor.NewMonitor(config)
	err := monitor.Start()
	if err != nil {
		log.Errorf("Failed to start monitor, err: %s", err)
		return
	}

	<-exitChan
}
