package monitor

import (
	_ "database/sql"
	"fmt"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/netx/tcp"
)

type Monitor struct {
	config  map[string]string
	manager *SessionManager
}

func NewMonitor(config map[string]string) *Monitor {
	return &Monitor{
		config: config,
	}
}

func (mnt *Monitor) SessionManager() *SessionManager {
	return mnt.manager
}

func (mnt *Monitor) Start() error {
	log.Info("================================Starting monitor=============================")

	// db := sqlx.MustConnect("mysql", "root:PASSWORD@tcp(l:3306)/sqlx_test")
	conf := mnt.config
	authStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", conf["mysql_user"], conf["mysql_password"],
		conf["mysql_host"], conf["mysql_port"], conf["mysql_db"])

	log.Infof("Connect to mysql db: %s", authStr)
	db, err := sqlx.Connect("mysql", authStr)
	if err != nil {
		log.Errorf("Failed to connect to mysql db: %s, err: %s", authStr, err)
		return err
	}

	port, err := strconv.Atoi(conf["port"])
	if err != nil {
		log.Errorf("Failed to parse listen port: %s, err: %s", conf["port"], err)
		return err
	}

	server := tcp.NewTcpServer(port)

	manager := NewSessionManager(db, server, conf["sampleUrl"])

	err = manager.Start()
	if err != nil {
		log.Errorf("Failed to start session manager, err: %s", err)
		return err
	}

	mnt.manager = manager
	return nil
}

func (mnt *Monitor) Stop() {
	log.Info("================================Stopping monitor=============================")
	if mnt.manager != nil {
		mnt.manager.Stop()
		mnt.manager = nil
	}
}
