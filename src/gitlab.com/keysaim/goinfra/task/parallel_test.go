package task

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func init() {
	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")
}

func TestParallel(t *testing.T) {

	require := require.New(t)
	p := NewParallel(false)
	for i := 0; i < 10; i += 1 {
		p.Add(func(args ...interface{}) (interface{}, error) {
			log.Infof("hello")
			return nil, nil
		})
	}
	rl, err := p.Run()
	require.Nil(err)
	require.Equal(len(rl), 10)
}

func TestParallelDoneFirst(t *testing.T) {

	require := require.New(t)
	p := NewParallel(true)
	for i := 0; i < 10; i += 1 {
		p.Add(func(args ...interface{}) (interface{}, error) {
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)+10))
			log.Infof("hello")
			return nil, nil
		})
	}
	rl, err := p.Run()
	require.Nil(err)
	require.Equal(len(rl), 1)
}
