package task

import (
	"time"

	"gitlab.com/keysaim/goinfra/log"
)

type Timer struct {
	tk     *time.Ticker
	tkDone chan struct{}
	ta     *time.Timer
}

func (tm *Timer) StartTick(t *Task, ms int) {
	tm.StopTick()
	tm.tk = time.NewTicker(time.Duration(ms) * time.Millisecond)
	tm.tkDone = make(chan struct{})

	go func(tk <-chan time.Time, done <-chan struct{}, t *Task) {
		for {
			select {
			case now := <-tk:
				log.Md("task").Debugf("run tick task %d at %v", t.id, now)
				t.run()
			case <-done:
				log.Md("task").Debug("tick done!")
				return
			}
		}
	}(tm.tk.C, tm.tkDone, t)

}

func (tm *Timer) StopTick() {
	if tm.tk != nil {
		tm.tk.Stop()
		tm.tkDone <- struct{}{}
		close(tm.tkDone)
		tm.tk = nil
		tm.tkDone = nil
	}
}

func (tm *Timer) StartTimer(t *Task, ms int) {
	tm.StopTimer()
	tm.ta = time.NewTimer(time.Duration(ms) * time.Millisecond)

	go func(ta <-chan time.Time, t *Task) {
		now := <-ta
		log.Md("task").Debugf("run after task %d at %v", t.id, now)
		t.run()
	}(tm.ta.C, t)
}

func (tm *Timer) StopTimer() {
	if tm.ta != nil {
		tm.ta.Stop()
		tm.ta = nil
	}
}
