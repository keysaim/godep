package task

import (
	"math/rand"
	"testing"
	"time"

	_ "github.com/stretchr/testify/assert"
	_ "github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

func doRandMath(num int) time.Duration {
	var value float64
	last := time.Now()

	for idx := 0; idx < num; idx++ {
		value += rand.Float64() * rand.Float64() / rand.Float64()
		time.Sleep(10000 * time.Nanosecond)
	}

	spent := time.Since(last)
	//log.Infof("do rand math %d times cost %f seconds with value %f", num, spent.Seconds(), value)
	return spent
}

type WorkerTestSuite struct {
	suite.Suite
}

func (ts *WorkerTestSuite) SetupSuite() {
	log.Register("task", log.ErrorLevel)
	log.SetDefLevel(log.ErrorLevel)
	log.Md("task").Info("SetupSuite")
}

func (ts *WorkerTestSuite) TearDownSuite() {
	log.Md("task").Info("TeardownSuite")
}

func (ts *WorkerTestSuite) skipWorkerLoopRun() {
	total := 1000
	last := time.Now()
	for idx := 0; idx < total; idx++ {
		doRandMath(1000)
	}
	spent := time.Since(last)
	log.Infof("total spent %f seconds, with total doRandMath %d times", spent.Seconds(), total)
}

func (ts *WorkerTestSuite) TestWorker() {
	//require := require.New(ts.T())

	resChan := make(chan *Result, 100)
	total := 1000
	totalTime := time.Duration(0)

	t := NewTask(resChan, func(...interface{}) (interface{}, error) {
		val := doRandMath(1000)
		return val, nil
	})

	worker := NewWorker("test worker", 20, 100)
	worker.Start()
	last := time.Now()
	go func() {
		for idx := 0; idx < total; idx++ {
			worker.AddTask(nil, t)
		}
	}()

	for idx := 0; idx < total; idx++ {
		res := <-resChan
		totalTime += res.Value.(time.Duration)
	}

	spent := time.Since(last)
	log.Infof("total spent %f seconds, total sum %f seconds, with total doRandMath %d times", spent.Seconds(), totalTime.Seconds(), total)
	worker.Stop()
}

func TestWorkerTestSuite_UT(t *testing.T) {
	suite.Run(t, new(WorkerTestSuite))
}

func runBench(b *testing.B, num int, bsize int) {
	log.Register("task", log.ErrorLevel)
	resChan := make(chan *Result)
	t := NewTask(resChan, func(...interface{}) (interface{}, error) {
		val := doRandMath(5000)
		return val, nil
	})

	worker := NewWorker("benchmark worker", num, bsize)
	worker.Start()

	exitChan := make(chan struct{})
	go func() {
		for i := 0; i < b.N; i++ {
			<-resChan
		}
		exitChan <- struct{}{}
	}()

	for i := 0; i < b.N; i++ {
		worker.AddTask(nil, t)
	}

	<-exitChan
	worker.Stop()
}

func BenchmarkNoRoutine(b *testing.B) {
	for i := 0; i < b.N; i++ {
		doRandMath(5000)
	}
}

func Benchmark10Routines100Buffer(b *testing.B) {
	runBench(b, 10, 100)
}

func Benchmark20Routines100Buffer(b *testing.B) {
	runBench(b, 20, 100)
}

func Benchmark50Routines100Buffer(b *testing.B) {
	runBench(b, 50, 100)
}

func Benchmark100Routines100Buffer(b *testing.B) {
	runBench(b, 100, 100)
}

func Benchmark200Routines100Buffer(b *testing.B) {
	runBench(b, 200, 100)
}

func Benchmark300Routines100Buffer(b *testing.B) {
	runBench(b, 300, 100)
}

func Benchmark400Routines100Buffer(b *testing.B) {
	runBench(b, 400, 100)
}

func Benchmark500Routines10Buffer(b *testing.B) {
	runBench(b, 500, 10)
}

func Benchmark500Routines50Buffer(b *testing.B) {
	runBench(b, 500, 50)
}

func Benchmark500Routines100Buffer(b *testing.B) {
	runBench(b, 500, 100)
}

func Benchmark500Routines200Buffer(b *testing.B) {
	runBench(b, 500, 200)
}

func Benchmark500Routines500Buffer(b *testing.B) {
	runBench(b, 500, 500)
}

func Benchmark500Routines1000Buffer(b *testing.B) {
	runBench(b, 500, 1000)
}

func Benchmark1000Routines100Buffer(b *testing.B) {
	runBench(b, 1000, 100)
}

func Benchmark2000Routines100Buffer(b *testing.B) {
	runBench(b, 2000, 100)
}

func Benchmark2000Routines1000Buffer(b *testing.B) {
	runBench(b, 2000, 1000)
}

func Benchmark10000Routines100Buffer(b *testing.B) {
	runBench(b, 10000, 100)
}

func Benchmark10000Routines1000Buffer(b *testing.B) {
	runBench(b, 10000, 1000)
}
