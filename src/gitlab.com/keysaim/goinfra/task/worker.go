package task

import (
	"sync"

	"gitlab.com/keysaim/goinfra/log"
)

type Worker struct {
	name     string
	number   int
	taskChan chan *Task
	quitChan chan struct{}
	exitChan chan struct{}
	wg       *sync.WaitGroup
}

func NewWorker(name string, numberRoutines int, bufferSize int) *Worker {

	log.Infof("Create worker %s with routines: %d, buffer size: %d", name, numberRoutines, bufferSize)
	return &Worker{
		name:     name,
		number:   numberRoutines,
		taskChan: make(chan *Task, bufferSize),
		quitChan: make(chan struct{}),
		exitChan: make(chan struct{}),
		wg:       &sync.WaitGroup{},
	}
}

func (w *Worker) Start() error {

	log.Infof("Start %s worker...", w.name)
	for idx := 0; idx < w.number; idx++ {
		w.wg.Add(1)
		go func(id int, taskChan <-chan *Task, quit <-chan struct{}) {
			defer w.wg.Done()

			for {
				select {
				case t := <-taskChan:
					log.Md("task").Debugf("worker \"%s\" routine %d run task %d once", w.name, id, t.id)
					t.run()
				case <-quit:
					log.Md("task").Debugf("worker \"%s\" routine %d quits", w.name, id)
					return
				}
			}
		}(idx, w.taskChan, w.quitChan)
	}

	go func() {
		w.wg.Wait()
		log.Md("task").Infof("All worker routines quited in worker %s", w.name)
		w.exitChan <- struct{}{}
	}()

	log.Infof("Worker %s started", w.name)

	return nil
}

func (w *Worker) Stop() {

	log.Infof("Stop worker %s...", w.name)
	close(w.quitChan)
	<-w.exitChan
	log.Md("task").Infof("Worker %s stopped", w.name)
}

func (w *Worker) Add(resChan chan<- *Result, taskFunc TaskFuncType, args ...interface{}) {

	t := NewTask(resChan, taskFunc, args...)
	w.taskChan <- t
}

func (w *Worker) AddTask(resChan chan<- *Result, t *Task) {

	w.taskChan <- t
}
