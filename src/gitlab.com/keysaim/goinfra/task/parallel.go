package task

import (
	"errors"
	"sync"
)

type Parallel struct {
	doneFirst bool
	doneChan  chan *Result
	queue     []*Task
	wg        sync.WaitGroup
}

func NewParallel(doneFirst bool) *Parallel {

	p := &Parallel{
		doneFirst: doneFirst,
	}
	return p
}

func (p *Parallel) Add(taskFunc TaskFuncType, args ...interface{}) {

	t := NewTask(nil, taskFunc, args...)
	p.queue = append(p.queue, t)
}

func (p *Parallel) Run() ([]*Result, error) {

	p.run()
	return p.Wait()
}

func (p *Parallel) AsyncRun() {
	p.Run()
}

func (p *Parallel) Wait() ([]*Result, error) {

	var err error

	if p.doneFirst {
		go func() {
			p.wg.Wait()
			close(p.doneChan)
		}()
		res := <-p.doneChan
		if res != nil {
			err = res.Err
		} else {
			err = errors.New("no task is successful")
		}
		return []*Result{res}, err
	}

	p.wg.Wait()

	var rl []*Result
	for _, t := range p.queue {
		rl = append(rl, t.res)
		if t.res != nil && t.res.Err != nil {
			err = t.res.Err
		}
	}
	return rl, err
}

func (p *Parallel) run() {

	if p.doneFirst {
		p.doneChan = make(chan *Result, len(p.queue))
	}
	for _, t := range p.queue {
		p.wg.Add(1)
		go func(t *Task) {
			t.run()
			if p.doneFirst && t.res != nil && t.res.Err == nil {
				p.doneChan <- t.res
			}
			p.wg.Done()
		}(t)
	}
}
