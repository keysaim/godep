package task

import (
	"math"
	"testing"
	"time"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type TimerTestSuite struct {
	suite.Suite
}

func (ts *TimerTestSuite) SetupSuite() {
	log.Register("task", log.DebugLevel)
	log.Md("task").Info("SetupSuite")
}

func (ts *TimerTestSuite) TearDownSuite() {
	log.Md("task").Info("TeardownSuite")
}

func (ts *TimerTestSuite) TestTickWithoutResChan() {
	require := require.New(ts.T())
	tm := Timer{}
	dr := 100 // ms

	log.Md("task").Info("testing tick")
	last := time.Now()
	t := NewTask(nil, func(...interface{}) (interface{}, error) {

		spent := time.Since(last)
		diff := math.Abs(spent.Seconds() - float64(dr)/1000)
		require.True(diff < 0.01)
		last = time.Now()
		return nil, nil
	})

	tm.StartTick(t, dr)
	time.Sleep(1 * time.Second)
	tm.StopTick()
	time.Sleep(1 * time.Second)
}

func (ts *TimerTestSuite) TestTickWithResChan() {
	require := require.New(ts.T())
	tm := Timer{}
	dr := 100 // ms

	log.Md("task").Info("testing tick")
	last := time.Now()
	resChan := make(chan *Result)
	t := NewTask(resChan, func(...interface{}) (interface{}, error) {

		spent := time.Since(last)
		diff := math.Abs(spent.Seconds() - float64(dr)/1000)
		last = time.Now()
		return diff, nil
	})

	tm.StartTick(t, dr)
	for idx := 0; idx < 5; idx++ {
		res := <-resChan
		diff := res.Value.(float64)
		log.Md("task").Info("spent %f", diff)
		require.True(diff < 0.01)
	}
	tm.StopTick()
	time.Sleep(1 * time.Second)
}

func (ts *TimerTestSuite) TestTimerWithoutResChan() {
	require := require.New(ts.T())
	tm := Timer{}
	dr := 100 // ms

	log.Md("task").Info("testing timer")
	count := 0
	last := time.Now()
	t := NewTask(nil, func(...interface{}) (interface{}, error) {
		count += 1
		spent := time.Since(last)
		diff := math.Abs(spent.Seconds() - float64(dr)/1000)
		require.True(diff < 0.01)
		last = time.Now()
		return nil, nil
	})

	tm.StartTimer(t, dr)
	time.Sleep(1 * time.Second)
	require.Equal(count, 1)
	tm.StopTimer()
	time.Sleep(1 * time.Second)
}

func TestTimerTestSuite_UT(t *testing.T) {
	suite.Run(t, new(TimerTestSuite))
}
