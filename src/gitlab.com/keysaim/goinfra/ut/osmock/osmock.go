package osmock

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"path"
	"runtime"
	"strings"

	"github4-chn.cisco.com/OMD/content_prepos/src/log"
)

var osFiles map[string]string

func init() {
	_, runfile, _, _ := runtime.Caller(1)
	dir := path.Dir(runfile)
}

func CreateTestServers() (*httptest.Server, *httptest.Server) {
	serverHandler := func(w http.ResponseWriter, r *http.Request) {
		log.Md("test").Debugf("server got one request %s", r.URL)

		filename := osFiles[r.URL.Path]
		if filename != "" {
			file, err := ioutil.ReadFile(filename)
			if err != nil {
				w.Write([]byte(fmt.Sprintf("no such file in server: %s", filename)))
			} else {
				w.Write(file)
			}
		} else {
			w.Write([]byte(fmt.Sprintf("hello from url: %s", r.URL)))
		}
	}
	server := httptest.NewServer(http.HandlerFunc(serverHandler))

	proxyHandler := func(w http.ResponseWriter, r *http.Request) {
		url := r.URL.String()
		proxyClient := &http.Client{}
		proxyReq, err := http.NewRequest(r.Method, url, r.Body)
		if err != nil {
			log.Md("test").Error("NewRequest failed with error", err)
			return
		}

		proxyReq.Header = r.Header
		if rsps, err := proxyClient.Do(proxyReq); err == nil {
			defer rsps.Body.Close()

			for key, vals := range rsps.Header {
				w.Header().Set(key, strings.Join(vals, ";"))
			}
			w.WriteHeader(rsps.StatusCode)
			io.Copy(w, rsps.Body)
		} else {
			log.Md("test").Error("proxy failed to get url", url, "with error", err)
		}
	}

	proxy := httptest.NewServer(http.HandlerFunc(proxyHandler))

	return server, proxy
}
