package db

import (
	"gitlab.com/keysaim/goinfra/log"
)

type DbOpType int
type ModelFields map[DbOpType][]string

const (
	DB_OP_ADD = 0
	DB_OP_PUT = 1
	DB_OP_GET = 2
)

type Model interface {
	ID() int64
	SetId(id int64)
	Table() string
	Fields(op DbOpType) []string
}

var modelMap = map[string]Model{}

// Please note, any operation related to the db should not use any handler here
var handlerMap = map[string]Handler{}

func RegisterModel(name string, m Model) {
	modelMap[name] = m
}

func RegisterHandler(name string, h Handler) {
	handlerMap[name] = h
}

func NewModel(name string) Model {
	h, ok := handlerMap[name]
	if !ok {
		log.Error("No such model name:", name)
		return nil
	}
	return h.New()
}

func NewModels(name string, len int) interface{} {
	h, ok := handlerMap[name]
	if !ok {
		log.Error("No such model name:", name)
		return nil
	}
	return h.Mnew(len)
}

func ModelNames() []string {

	names := make([]string, 0, len(modelMap))
	for k, _ := range modelMap {
		names = append(names, k)
	}
	return names
}

func TableName(name string) string {

	m, ok := modelMap[name]
	if !ok {
		log.Errorf("Failed to get table name, unknown type name: %s", name)
		return ""
	}
	return m.Table()
}

func TableFields(name string, op DbOpType) []string {

	m, ok := modelMap[name]
	if !ok {
		log.Errorf("Failed to get table fields, unknown name type: %s", name)
		return nil
	}

	return m.Fields(op)
}

func GetFields(fieldsMap ModelFields, op DbOpType) []string {

	fds, ok := fieldsMap[op]
	if !ok {
		log.Debug("Not set for the DbOpType: %d", op)
		return nil
	}

	return fds
}
