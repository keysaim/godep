package db

import (
	"github.com/jmoiron/sqlx"
)

type Context interface {
	GetHandler(name string) Handler
	DB() *sqlx.DB
}
