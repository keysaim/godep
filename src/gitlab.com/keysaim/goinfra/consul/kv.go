package consul

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	capi "github.com/hashicorp/consul/api"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/typex"
)

var ErrNotExist error = errors.New("Consul key not exists")
var ErrCasFail error = errors.New("Consul CAS fail")
var ErrRolledBack error = errors.New("Consul TX rolled back")
var ErrLockIsHold error = errors.New("Consul lock is hold")
var ErrLockIsFrozen error = errors.New("Consul lock is frozen")

type ConsulKV struct {
	kv *capi.KV
}

func NewConsulKV(kv *capi.KV) *ConsulKV {
	return &ConsulKV{
		kv: kv,
	}
}

func (kv *ConsulKV) KV() *capi.KV {
	return kv.kv
}

func (kv *ConsulKV) Incr(path string) (int64, error) {

	return kv.IncrBy(path, int64(1))
}

func (kv *ConsulKV) TryIncr(path string, retries int, wait int) (int64, error) {

	return kv.TryIncrBy(path, int64(1), retries, wait)
}

func (kv *ConsulKV) Decr(path string) (int64, error) {

	return kv.IncrBy(path, int64(-1))
}

func (kv *ConsulKV) TryDecr(path string, retries int, wait int) (int64, error) {

	return kv.TryIncrBy(path, int64(-1), retries, wait)
}

func (kv *ConsulKV) IncrBy(path string, add int64) (int64, error) {

	p, _, err := kv.kv.Get(path, nil)
	if err != nil {
		return int64(0), err
	}
	if p == nil {
		vs := strconv.FormatInt(add, 10)
		ok, _, _ := kv.kv.CAS(&capi.KVPair{
			Key:   path,
			Value: []byte(vs),
		}, nil)
		if ok {
			return int64(1), nil
		}
	} else {
		cnt, err := strconv.ParseInt(string(p.Value), 10, 64)
		if err != nil {
			return int64(0), err
		}
		cnt += add
		vs := strconv.FormatInt(cnt, 10)
		ok, _, _ := kv.kv.CAS(&capi.KVPair{
			Key:         path,
			Value:       []byte(vs),
			ModifyIndex: p.ModifyIndex,
		}, nil)
		if ok {
			return cnt, nil
		}
	}
	return int64(0), ErrCasFail
}

func (kv *ConsulKV) TryIncrBy(path string, add int64, retries int, wait int) (int64, error) {

	for i := 0; i < retries; i += 1 {
		if i > 0 {
			s := rand.Intn(wait)
			time.Sleep(time.Duration(s) * time.Millisecond)
		}
		cnt, err := kv.IncrBy(path, add)
		if err != nil && err != ErrCasFail {
			return int64(0), err
		}
		if err == nil {
			return cnt, nil
		}
	}

	return int64(0), ErrCasFail
}

func (kv *ConsulKV) FreezeLockTime(path string) int64 {

	now := time.Now().Unix()
	b, _ := kv.Get(path + "-lockTime")
	if len(b) == 0 {
		return now
	}
	v, _ := strconv.ParseInt(string(b), 10, 64)
	return time.Now().Unix() - v
}

func (kv *ConsulKV) FreezeLock(path, owner string, expire int) (string, error) {

	if kv.FreezeLockTime(path) < int64(expire) {
		return "", ErrLockIsFrozen
	}

	o, err := kv.Lock(path, owner, expire)
	if err != nil {
		return o, err
	}
	s := strconv.FormatInt(time.Now().Unix(), 10)
	kv.Put(path+"-lockTime", []byte(s))
	return o, nil
}

func (kv *ConsulKV) Lock(path, owner string, expire int) (string, error) {

	p, _, err := kv.kv.Get(path, nil)
	if err != nil {
		return "", err
	}
	if p == nil {
		val := fmt.Sprintf("%s:%d", owner, time.Now().Unix()+int64(expire))
		ok, _, _ := kv.kv.CAS(&capi.KVPair{
			Key:   path,
			Value: []byte(val),
		}, nil)
		if ok {
			return owner, nil
		}
	} else {
		segs := strings.Split(string(p.Value), ":")
		co := segs[0]
		ct := int64(0)
		if len(segs) > 1 {
			ct, _ = strconv.ParseInt(segs[1], 10, 64)
		}
		now := time.Now().Unix()
		if co != owner {
			if ct > now {
				return co, ErrLockIsHold
			}
			log.Debugf("The lock %s expired, %d", path, ct)
		}
		val := fmt.Sprintf("%s:%d", owner, now+int64(expire))
		ok, _, _ := kv.kv.CAS(&capi.KVPair{
			Key:         path,
			Value:       []byte(val),
			ModifyIndex: p.ModifyIndex,
		}, nil)
		if ok {
			return owner, nil
		}
	}

	p, _, _ = kv.kv.Get(path, nil)
	if p != nil {
		return strings.Split(string(p.Value), ":")[0], ErrLockIsHold
	} else {
		return "", errors.New("Unexpected consul lock error")
	}
}

func (kv *ConsulKV) Unlock(path, owner string) (string, error) {

	p, _, err := kv.kv.Get(path, nil)
	if err != nil {
		return "", err
	}
	if p == nil {
		return "", nil
	}

	co := strings.Split(string(p.Value), ":")[0]
	if co != owner {
		return co, ErrLockIsHold
	}

	ok, _, err := kv.kv.DeleteCAS(&capi.KVPair{
		Key:         path,
		ModifyIndex: p.ModifyIndex,
	}, nil)
	if ok {
		return "", nil
	}

	p, _, _ = kv.kv.Get(path, nil)
	if p != nil {
		return strings.Split(string(p.Value), ":")[0], ErrLockIsHold
	} else {
		return "", errors.New("Unexpected consul unlock error")
	}
}

func (kv *ConsulKV) Keys(prefix string, recursive bool) ([]string, error) {

	s := "/"
	if recursive {
		s = ""
	}
	keys, _, err := kv.kv.Keys(prefix, s, nil)
	if err != nil {
		return nil, err
	}
	pl := len(prefix)
	var kl []string
	for _, k := range keys {
		if k == prefix {
			continue
		}
		end := len(k)
		if end == pl {
			continue
		}
		if k[end-1] == "/"[0] {
			end -= 1
		}
		k = k[pl:end]
		kl = append(kl, k)
	}
	return kl, nil
}

func (kv *ConsulKV) ListLevel1(prefix string) (map[string]string, error) {

	kvpl, _, err := kv.kv.List(prefix, nil)
	if err != nil {
		return nil, err
	}

	pl := len(prefix)
	m := map[string]string{}
	for _, kvp := range kvpl {
		k := kvp.Key
		v := kvp.Value
		end := len(k)
		if end == pl {
			continue
		}
		if k[end-1] == "/"[0] {
			end -= 1
		}
		k = k[pl:end]
		m[k] = string(v)
	}

	return m, nil
}

func (kv *ConsulKV) ListLevel2(prefix string) (map[string]map[string]string, error) {

	kvpl, _, err := kv.kv.List(prefix, nil)
	if err != nil {
		return nil, err
	}

	pl := len(prefix)
	mm := map[string]map[string]string{}
	for _, kvp := range kvpl {
		k := kvp.Key
		v := kvp.Value
		end := len(k)
		if end == pl {
			continue
		}
		if k[end-1] == "/"[0] {
			end -= 1
		}
		k = k[pl:end]

		segs := strings.Split(k, "/")
		if len(segs) < 2 {
			// not level2 key, ignore
			continue
		}
		k1 := segs[0]
		k2 := segs[1]
		m, ok := mm[k1]
		if !ok {
			m = map[string]string{}
			mm[k1] = m
		}
		m[k2] = string(v)
	}

	return mm, nil
}

func (kv *ConsulKV) PutMap(prefix string, m map[string]string) error {

	var txn capi.KVTxnOps
	for k, v := range m {
		op := &capi.KVTxnOp{
			Verb:  capi.KVSet,
			Key:   prefix + k,
			Value: []byte(v),
		}
		txn = append(txn, op)
	}
	if len(txn) == 0 {
		return nil
	}

	_, err := kv.RunTx(txn)
	return err
}

func (kv *ConsulKV) PutMapMap(prefix string, mm map[string]map[string]string) error {

	var txn capi.KVTxnOps
	for k1, m := range mm {
		for k2, v := range m {
			op := &capi.KVTxnOp{
				Verb:  capi.KVSet,
				Key:   prefix + k1 + "/" + k2,
				Value: []byte(v),
			}
			txn = append(txn, op)
		}
	}
	if len(txn) == 0 {
		return nil
	}

	_, err := kv.RunTx(txn)
	return err
}

func (kv *ConsulKV) RunTx(txn capi.KVTxnOps) ([]*capi.KVPair, error) {

	var kvpl []*capi.KVPair
	for len(txn) > 0 {
		stxn := txn
		if len(txn) > 64 {
			stxn = txn[:64]
			txn = txn[64:]
		} else {
			txn = nil
		}
		ok, rsp, _, err := kv.kv.Txn(stxn, nil)
		if err != nil {
			return nil, err
		}
		if !ok {
			return nil, ErrRolledBack
		}
		kvpl = append(kvpl, rsp.Results...)
	}
	return kvpl, nil
}

func (kv *ConsulKV) Put(key string, value []byte) error {

	_, err := kv.kv.Put(&capi.KVPair{
		Key:   key,
		Value: value,
	}, nil)

	return err
}

func (kv *ConsulKV) GetLevel2(prefix string, keys []string) ([]map[string]string, error) {

	var ml []map[string]string
	for _, k := range keys {
		m, err := kv.ListLevel1(prefix + k + "/")
		if err != nil {
			return nil, err
		}
		ml = append(ml, m)
	}
	return ml, nil
}

func (kv *ConsulKV) Gets(prefix string, keys []string) ([]string, error) {

	var txn capi.KVTxnOps
	for _, k := range keys {
		op := &capi.KVTxnOp{
			Verb: capi.KVGet,
			Key:  prefix + k,
		}
		txn = append(txn, op)
	}
	if len(txn) == 0 {
		return nil, nil
	}

	kvpl, err := kv.RunTx(txn)
	if err != nil {
		return nil, err
	}
	var vl []string
	for _, kvp := range kvpl {
		vl = append(vl, string(kvp.Value))
	}
	return vl, nil
}

func (kv *ConsulKV) Get(key string) ([]byte, error) {

	p, _, err := kv.kv.Get(key, nil)
	if err != nil {
		return nil, err
	}
	if p == nil {
		return nil, ErrNotExist
	}
	return p.Value, nil
}

func (kv *ConsulKV) Del(key string) error {

	_, err := kv.kv.Delete(key, nil)
	return err
}

func (kv *ConsulKV) DelTree(prefix string) error {

	_, err := kv.kv.DeleteTree(prefix, nil)
	return err
}

func (kv *ConsulKV) DelTrees(prefix string, keys []string) error {

	var txn capi.KVTxnOps
	for _, k := range keys {
		op := &capi.KVTxnOp{
			Verb: capi.KVDeleteTree,
			Key:  prefix + k,
		}
		txn = append(txn, op)
	}
	if len(txn) == 0 {
		return nil
	}

	_, err := kv.RunTx(txn)
	if err != nil {
		return err
	}

	return nil
}

func (kv *ConsulKV) PutObj(key string, obj interface{}) error {

	value, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	return kv.Put(key, value)
}

func (kv *ConsulKV) PutObjs(objs typex.JsonMap) error {

	m := map[string]string{}
	for k, obj := range objs {
		value, err := json.Marshal(obj)
		if err != nil {
			return err
		}
		m[k] = string(value)
	}
	return kv.PutMap("", m)
}

func (kv *ConsulKV) GetObjs(objs typex.JsonMap) error {

	kl := []string{}
	for k, _ := range objs {
		kl = append(kl, k)
	}
	vl, err := kv.Gets("", kl)
	if err != nil {
		return err
	}

	for i, v := range vl {
		obj := objs[kl[i]]
		err = json.Unmarshal([]byte(v), obj)
		if err != nil {
			return err
		}
	}

	return nil
}

func (kv *ConsulKV) ListObjs(prefix string, newFunc func() interface{}) (map[string]interface{}, error) {

	m, err := kv.ListLevel1(prefix)
	if err != nil {
		return nil, err
	}

	objMap := map[string]interface{}{}
	for k, v := range m {
		obj := newFunc()
		err = json.Unmarshal([]byte(v), obj)
		if err != nil {
			return nil, err
		}
		objMap[k] = obj
	}

	return objMap, nil
}

func (kv *ConsulKV) GetObj(obj interface{}, key string) error {

	value, err := kv.Get(key)
	if err != nil {
		return err
	}

	err = json.Unmarshal(value, obj)
	if err != nil {
		return err
	}

	return nil
}

func (kv *ConsulKV) UpdateObjUnsafe(key string, fields typex.JsonMap) error {

	obj := typex.JsonMap{}
	err := kv.GetObj(&obj, key)
	if err != nil {
		return err
	}

	for k, v := range fields {
		obj[k] = v
	}

	return kv.PutObj(key, obj)
}

func (kv *ConsulKV) UpdateObj(key string, fields typex.JsonMap) error {

	p, _, err := kv.kv.Get(key, nil)
	if err != nil {
		return err
	}

	if p == nil {
		return ErrNotExist
	}

	obj := typex.JsonMap{}
	err = json.Unmarshal([]byte(p.Value), &obj)
	if err != nil {
		return err
	}

	for k, v := range fields {
		obj[k] = v
	}

	p.Value, err = json.Marshal(&obj)
	if err != nil {
		return err
	}

	ok, _, err := kv.kv.CAS(p, nil)
	if err != nil {
		return err
	}
	if !ok {
		return ErrCasFail
	}
	return nil
}

func (kv *ConsulKV) TryUpdateObj(key string, fields typex.JsonMap, tries int) error {

	for i := 0; i < tries; i++ {
		err := kv.UpdateObj(key, fields)
		if err == nil {
			return nil
		}
		if err != ErrCasFail {
			return err
		}
	}
	return ErrCasFail
}

// like map[string]map[string]string
type ConsulObjMap struct {
	prefix string
	kv     *ConsulKV
}

func NewConsulObjMap(prefix string, kv *ConsulKV) *ConsulObjMap {
	return &ConsulObjMap{
		prefix: prefix,
		kv:     kv,
	}
}

func (s *ConsulObjMap) Put(key string, obj map[string]string) error {

	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.Puts(obj)
}

func (s *ConsulObjMap) Puts(mm map[string]map[string]string) error {

	return s.kv.PutMapMap(s.prefix, mm)
}

func (s *ConsulObjMap) Get(key string) (map[string]string, error) {

	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.All()
}

func (s *ConsulObjMap) GetField(key string, field string) (string, error) {
	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.Get(field)
}

func (s *ConsulObjMap) GetFields(key string, fields []string) ([]string, error) {
	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.Gets(fields)
}

func (s *ConsulObjMap) Gets(keys []string) ([]map[string]string, error) {

	return s.kv.GetLevel2(s.prefix, keys)
}

func (s *ConsulObjMap) Picks(keys []string, field string) ([]string, error) {

	var nkl []string
	for _, k := range keys {
		nkl = append(nkl, k+"/"+field)
	}

	return s.kv.Gets(s.prefix, nkl)
}

func (s *ConsulObjMap) Values() ([]map[string]string, error) {

	mm, err := s.kv.ListLevel2(s.prefix)
	if err != nil {
		return nil, err
	}
	var ml []map[string]string
	for _, m := range mm {
		ml = append(ml, m)
	}

	return ml, nil
}

func (s *ConsulObjMap) All() (map[string]map[string]string, error) {

	return s.kv.ListLevel2(s.prefix)
}

func (s *ConsulObjMap) Keys() ([]string, error) {

	return s.kv.Keys(s.prefix, false)
}

func (s *ConsulObjMap) Has(key string) bool {

	key = s.prefix + key
	_, err := s.kv.Get(key)
	return err == nil
}

func (s *ConsulObjMap) DelField(key, key2 string) error {

	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.Del(key2)
}

func (s *ConsulObjMap) Del(key string) error {

	om := NewConsulMap(s.prefix+key+"/", s.kv)
	return om.Clear()
}

func (s *ConsulObjMap) Dels(keys []string) error {

	return s.kv.DelTrees(s.prefix, keys)
}

func (s *ConsulObjMap) Clear() error {

	return s.kv.DelTree(s.prefix)
}

// like map[string]string
type ConsulMap struct {
	prefix string
	kv     *ConsulKV
}

func NewConsulMap(prefix string, kv *ConsulKV) *ConsulMap {
	return &ConsulMap{
		prefix: prefix,
		kv:     kv,
	}
}

func (m *ConsulMap) Put(key string, value string) error {

	key = m.prefix + key
	return m.kv.Put(key, []byte(value))
}

func (m *ConsulMap) Get(key string) (string, error) {

	key = m.prefix + key
	value, err := m.kv.Get(key)
	if err != nil {
		return "", err
	}
	return string(value), nil
}

func (m *ConsulMap) Gets(keys []string) ([]string, error) {

	return m.kv.Gets(m.prefix, keys)
}

func (m *ConsulMap) Del(key string) error {

	key = m.prefix + key
	return m.kv.Del(key)
}

func (m *ConsulMap) Puts(fields map[string]string) error {

	return m.kv.PutMap(m.prefix, fields)
}

func (m *ConsulMap) Keys() ([]string, error) {

	return m.kv.Keys(m.prefix, true)
}

func (m *ConsulMap) Values() ([]string, error) {

	o, err := m.All()
	if err != nil {
		return nil, err
	}

	var vl []string
	for _, v := range o {
		vl = append(vl, v)
	}
	return vl, nil
}

func (m *ConsulMap) All() (map[string]string, error) {

	return m.kv.ListLevel1(m.prefix)
}

func (m *ConsulMap) Clear() error {

	err := m.kv.DelTree(m.prefix)
	if err != nil {
		return err
	}

	return m.kv.Del(m.prefix)
}

// like map[string]map[string]map[string]string
type ConsulObjMapMap struct {
	prefix string
	kv     *ConsulKV
}

func NewConsulObjMapMap(prefix string, kv *ConsulKV) *ConsulObjMapMap {
	return &ConsulObjMapMap{
		prefix: prefix,
		kv:     kv,
	}
}

func (s *ConsulObjMapMap) Put(key1, key2 string, obj map[string]string) error {

	om := NewConsulMap(s.prefix+key1+"/"+key2+"/", s.kv)
	return om.Puts(obj)
}

func (s *ConsulObjMapMap) Get(key1, key2 string) (map[string]string, error) {

	om := NewConsulMap(s.prefix+key1+"/"+key2+"/", s.kv)
	return om.All()
}

func (s *ConsulObjMapMap) Gets(keys [][2]string) ([]map[string]string, error) {

	ol := []map[string]string{}
	for _, kl := range keys {
		om := NewConsulMap(s.prefix+kl[0]+"/"+kl[1]+"/", s.kv)
		o, err := om.All()
		if err != nil {
			return nil, err
		}
		ol = append(ol, o)
	}
	return ol, nil
}

func (s *ConsulObjMapMap) Has(key1, key2 string) bool {

	key := s.prefix + key1 + "/" + key2 + "/"
	_, err := s.kv.Get(key)
	return err == nil
}

func (s *ConsulObjMapMap) Del(key1, key2 string) error {

	om := NewConsulMap(s.prefix+key1+"/"+key2+"/", s.kv)
	return om.Clear()
}

// behave like map[string]interface{}, but in consul, store it as a json string
type ConsulJsonMap struct {
	prefix string
	kv     *ConsulKV
}

func NewConsulJsonMap(prefix string, kv *ConsulKV) *ConsulJsonMap {
	return &ConsulJsonMap{
		prefix: prefix,
		kv:     kv,
	}
}

func (m *ConsulJsonMap) PutObj(obj interface{}) error {
	return m.kv.PutObj(m.prefix, obj)
}

func (m *ConsulJsonMap) GetObj(obj interface{}) error {
	err := m.kv.GetObj(obj, m.prefix)
	if err != nil {
		return err
	}
	return nil
}

func (m *ConsulJsonMap) Put(key string, value interface{}) error {

	return m.Puts(typex.JsonMap{key: value})
}

func (m *ConsulJsonMap) Puts(fields typex.JsonMap) error {

	return m.kv.UpdateObj(m.prefix, fields)
}

func (m *ConsulJsonMap) Get(key string) (interface{}, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	v, _ := obj[key]
	return v, nil
}

func (m *ConsulJsonMap) GetString(key string) (string, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return "", err
	}
	return obj.GetString(key), nil
}

func (m *ConsulJsonMap) GetInt(key string) (int, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return 0, err
	}
	return obj.GetInt(key), nil
}

func (m *ConsulJsonMap) GetBool(key string) (bool, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return false, err
	}
	return obj.GetBool(key), nil
}

func (m *ConsulJsonMap) GetStrings(keys []string) ([]string, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	vl := []string{}
	for _, k := range keys {
		vl = append(vl, obj.GetString(k))
	}
	return vl, nil
}

func (m *ConsulJsonMap) GetInts(keys []string) ([]int, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	vl := []int{}
	for _, k := range keys {
		vl = append(vl, obj.GetInt(k))
	}
	return vl, nil
}

func (m *ConsulJsonMap) Gets(keys []string) ([]interface{}, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	vl := []interface{}{}
	for _, k := range keys {
		v, _ := obj[k]
		vl = append(vl, v)
	}

	return vl, nil
}

func (m *ConsulJsonMap) Del(key string) error {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return err
	}

	delete(obj, key)
	return m.PutObj(obj)
}

func (m *ConsulJsonMap) Keys() ([]string, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	kl := []string{}
	for k, _ := range obj {
		kl = append(kl, k)
	}
	return kl, nil
}

func (m *ConsulJsonMap) Values() ([]interface{}, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	vl := []interface{}{}
	for _, v := range obj {
		vl = append(vl, v)
	}
	return vl, nil
}

func (m *ConsulJsonMap) All() (typex.JsonMap, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(&obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (m *ConsulJsonMap) Clear() error {

	err := m.kv.DelTree(m.prefix)
	if err != nil {
		return err
	}

	return m.kv.Del(m.prefix)
}

// map[string]ConsulJsonMap
type ConsulMapJsonMap struct {
	prefix string
	kv     *ConsulKV
}

func NewConsulMapJsonMap(prefix string, kv *ConsulKV) *ConsulMapJsonMap {
	return &ConsulMapJsonMap{
		prefix: prefix,
		kv:     kv,
	}
}

func (m *ConsulMapJsonMap) Put(key string, value interface{}) error {

	jm := NewConsulJsonMap(m.prefix+key, m.kv)
	return jm.PutObj(value)
}

func (m *ConsulMapJsonMap) Puts(fields typex.JsonMap) error {

	nm := typex.JsonMap{}
	for k, obj := range fields {
		nm[m.prefix+k] = obj
	}

	return m.kv.PutObjs(nm)
}

func (m *ConsulMapJsonMap) Get(key string) (typex.JsonMap, error) {

	obj := typex.JsonMap{}
	err := m.GetObj(key, &obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (m *ConsulMapJsonMap) GetObj(key string, obj interface{}) error {

	jm := NewConsulJsonMap(m.prefix+key, m.kv)
	return jm.GetObj(obj)
}

func (m *ConsulMapJsonMap) GetStringObj(key string) (map[string]string, error) {

	obj := map[string]string{}
	err := m.GetObj(key, &obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (m *ConsulMapJsonMap) Gets(keys []string) ([]typex.JsonMap, error) {

	objs := typex.JsonMap{}
	for _, k := range keys {
		objs[m.prefix+k] = &typex.JsonMap{}
	}

	err := m.kv.GetObjs(objs)
	if err != nil {
		return nil, err
	}
	vl := []typex.JsonMap{}
	for _, k := range keys {
		vl = append(vl, *objs[m.prefix+k].(*typex.JsonMap))
	}

	return vl, nil
}

func (m *ConsulMapJsonMap) GetObjs(keys []string, newFunc func() interface{}) ([]interface{}, error) {

	objs := typex.JsonMap{}
	for _, k := range keys {
		objs[m.prefix+k] = newFunc()
	}

	err := m.kv.GetObjs(objs)
	if err != nil {
		return nil, err
	}
	vl := []interface{}{}
	for _, k := range keys {
		vl = append(vl, objs[m.prefix+k])
	}

	return vl, nil
}

func (m *ConsulMapJsonMap) Del(key string) error {

	jm := NewConsulJsonMap(m.prefix+key, m.kv)
	return jm.Clear()
}

func (m *ConsulMapJsonMap) Keys() ([]string, error) {

	om, err := m.kv.ListLevel1(m.prefix)
	if err != nil {
		return nil, err
	}
	kl := []string{}
	for k, _ := range om {
		kl = append(kl, k)
	}
	return kl, nil
}

func (m *ConsulMapJsonMap) Values() ([]typex.JsonMap, error) {

	ol, err := m.All()
	if err != nil {
		return nil, err
	}
	vl := []typex.JsonMap{}
	for _, o := range ol {
		vl = append(vl, o)
	}
	return vl, nil
}

func (m *ConsulMapJsonMap) All() (map[string]typex.JsonMap, error) {

	om, err := m.kv.ListObjs(m.prefix, func() interface{} {
		return &typex.JsonMap{}
	})
	if err != nil {
		return nil, err
	}

	jom := map[string]typex.JsonMap{}
	for k, o := range om {
		jom[k] = *o.(*typex.JsonMap)
	}

	return jom, nil
}

func (m *ConsulMapJsonMap) AllObjs(newFunc func() interface{}) (map[string]interface{}, error) {

	return m.kv.ListObjs(m.prefix, newFunc)
}

func (m *ConsulMapJsonMap) Clear() error {

	err := m.kv.DelTree(m.prefix)
	if err != nil {
		return err
	}

	return m.kv.Del(m.prefix)
}
