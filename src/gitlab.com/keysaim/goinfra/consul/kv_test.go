package consul

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	capi "github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/typex"
)

var kv *ConsulKV

func init() {
	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")

	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	if err != nil {
		log.Fatal(err)
	}
	kv = NewConsulKV(c.KV())
}

func TestConsulMap(t *testing.T) {

	require := require.New(t)
	cm := NewConsulMap("test/case1/", kv)

	m := map[string]string{
		"test": "hello",
		"this": "not ok",
		"haha": "ho ho",
	}

	err := cm.Clear()
	require.Nil(err)

	err = cm.Puts(m)
	require.Nil(err)

	keys, err := cm.Keys()
	require.Nil(err)
	require.Equal(len(keys), len(m))
	for _, k := range keys {
		_, ok := m[k]
		require.True(ok)
	}

	values, err := cm.Values()
	require.Nil(err)
	require.Equal(len(values), len(m))

	nm, err := cm.All()
	require.Nil(err)
	require.Equal(len(nm), len(m))
	for k, v := range nm {
		v1, _ := m[k]
		require.Equal(v, v1)
	}

	vl, err := cm.Gets([]string{"test", "this"})
	require.Nil(err)
	require.Equal(vl[0], m["test"])
	require.Equal(vl[1], m["this"])
}

func TestConsulObjMap(t *testing.T) {

	require := require.New(t)
	cm := NewConsulObjMap("test/case2/", kv)

	mm := map[string]map[string]string{
		"obj1": map[string]string{
			"hello": "world",
			"this":  "not ok",
			"id":    "1",
		},
		"obj2": map[string]string{
			"haha": "hoho",
			"hehe": "666",
			"id":   "2",
		},
	}

	err := cm.Clear()
	require.Nil(err)

	err = cm.Puts(mm)
	require.Nil(err)

	keys, err := cm.Keys()
	require.Nil(err)
	for _, k := range keys {
		_, ok := mm[k]
		require.True(ok)
	}

	values, err := cm.Values()
	require.Nil(err)
	require.Equal(len(values), len(mm))

	nmm, err := cm.All()
	for k1, nm := range nmm {
		m, ok := nmm[k1]
		require.True(ok)
		require.Equal(nm, m)
	}

	ml, err := cm.Gets([]string{"obj1", "obj2"})
	require.Nil(err)
	require.Equal(len(ml), 2)
	require.Equal(ml[0], mm["obj1"])
	require.Equal(ml[1], mm["obj2"])

	_, err = cm.Picks([]string{"obj1", "notexist"}, "id")
	require.NotNil(err)

	ml, err = cm.Gets([]string{"obj1", "notexist"})
	require.Nil(err)
	require.Equal(len(ml), 2)
	require.Equal(ml[0], mm["obj1"])
	require.Equal(len(ml[1]), 0)

	m, err := cm.Get("notexist")
	require.Nil(err)
	require.Equal(len(m), 0)
}

func TestConsulJsonMap(t *testing.T) {

	require := require.New(t)
	key := "test/TestConsulJsonMap/"
	cm := NewConsulJsonMap(key, kv)

	m := map[string]string{
		"test": "hello",
		"this": "not ok",
		"haha": "ho ho",
	}

	err := cm.Clear()
	require.Nil(err)

	err = cm.PutObj(m)
	require.Nil(err)
	obj := typex.JsonMap{}
	err = cm.GetObj(&obj)
	require.Nil(err)
	require.Equal(m["test"], obj["test"])
	err = cm.Put("test2", "value2")
	require.Nil(err)
	v, err := cm.Get("test2")
	sv, ok := v.(string)
	require.True(ok)
	require.Equal("value2", sv)
	sv, err = cm.GetString("test2")
	require.Nil(err)
	require.Equal("value2", sv)

	cm.Put("k3", 100)
	iv, err := cm.GetInt("k3")
	require.Nil(err)
	require.Equal(100, iv)
	iv, err = cm.GetInt("notexist")
	require.Nil(err)
	require.Equal(0, iv)

	svl, err := cm.GetStrings([]string{"test", "test2", "notexist"})
	require.Nil(err)
	require.Equal([]string{"hello", "value2", ""}, svl)
}

func TestConsulMapJsonMap(t *testing.T) {

	require := require.New(t)
	key := "test/TestConsulJsonMap/"
	cmm := NewConsulMapJsonMap(key, kv)

	m := map[string]string{
		"test": "hello",
		"this": "not ok",
		"haha": "ho ho",
	}

	err := cmm.Clear()
	require.Nil(err)
	err = cmm.Put("o1", m)
	require.Nil(err)
	sm, err := cmm.GetStringObj("o1")
	require.Nil(err)
	require.Equal(m, sm)

	err = cmm.Puts(typex.JsonMap{
		"o2": m,
		"o3": m,
		"o4": m,
		"o5": m,
	})
	require.Nil(err)
	kl, err := cmm.Keys()
	require.Nil(err)
	require.Equal(5, len(kl))
	vl, err := cmm.Values()
	require.Nil(err)
	require.Equal(5, len(vl))
	require.Equal(3, len(vl[4]))
	require.Equal("hello", vl[4]["test"])

	ol, err := cmm.AllObjs(func() interface{} {
		return &map[string]string{}
	})
	require.Nil(err)
	require.Equal(5, len(ol))
}

func TestConsulLock(t *testing.T) {

	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")
	require := require.New(t)

	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	require.Nil(err)
	kv := NewConsulKV(c.KV())
	path := "test/lock"
	kv.KV().Delete(path, nil)

	owner1 := "owner1"
	owner2 := "owner2"
	co, err := kv.Lock(path, owner1, 1)
	require.Nil(err)
	require.Equal(owner1, co)

	co, err = kv.Lock(path, owner2, 1)
	require.Equal(ErrLockIsHold, err)
	require.Equal(owner1, co)

	co, err = kv.Unlock(path, owner2)
	require.Equal(ErrLockIsHold, err)
	require.Equal(owner1, co)
	time.Sleep(1020 * time.Millisecond)

	co, err = kv.Lock(path, owner2, 1)
	require.Nil(err)
	require.Equal(owner2, co)

	co, err = kv.Unlock(path, owner2)
	require.Nil(err)
	require.Equal("", co)

	p, _, err := kv.KV().Get(path, nil)
	require.Nil(err)
	require.Nil(p)

	var succ int32
	wg := sync.WaitGroup{}
	wg.Add(20)
	for i := 0; i < 20; i += 1 {
		go func(i int) {
			o := fmt.Sprintf("o%d", i)
			co, err := kv.Lock(path, o, 1)
			if err == nil {
				atomic.AddInt32(&succ, int32(1))
			} else {
				log.Infof("%d failed to lock, the lock is hold by %s", i, co)
			}
			wg.Done()
		}(i)
	}
	wg.Wait()
	require.Equal(int32(1), succ)
}
