package consul

import (
	"context"
	"errors"
	"time"

	capi "github.com/hashicorp/consul/api"
	"gitlab.com/keysaim/goinfra/log"
)

type Elector struct {
	ctx        context.Context
	cancel     context.CancelFunc
	node       string
	key        string
	checkIntvl int
	c          *capi.Client
	kv         *ConsulKV
}

func NewElector(pctx context.Context, c *capi.Client, node, key string, checkIntvl int) *Elector {

	e := &Elector{
		node:       node,
		key:        key,
		checkIntvl: checkIntvl,
		c:          c,
		kv:         NewConsulKV(c.KV()),
	}
	e.ctx, e.cancel = context.WithCancel(pctx)
	return e
}

func (e *Elector) Start() error {

	if e.node == "" {
		node, err := e.c.Agent().NodeName()
		if err != nil {
			log.Error("Failed to get the node name: ", err)
			return err
		}
		if node == "" {
			log.Error("Empty node name from consul agent")
			return errors.New("Empty node name from consul agent")
		}
		e.node = node
	}
	go e.elect()
	return nil
}

func (e *Elector) Stop() {

	e.cancel()
}

func (e *Elector) IsLeader() bool {

	session := e.getSession(e.key)
	kv, _, err := e.kv.KV().Get(e.key, nil)
	if err != nil {
		log.Error(err)
		return false
	}
	if kv == nil {
		log.Warnf("The leader key %s not exist", e.key)
		return false
	}

	return e.node == string(kv.Value) && session == kv.Session
}

func (e *Elector) elect() {

	log.Infof("Elect loop for %s starts", e.key)
	for {
		select {
		case <-e.ctx.Done():
			log.Info("Election loop was canceled")
		default:
			if !e.IsLeader() {
				session := e.getSession(e.key)
				pair := &capi.KVPair{
					Key:     e.key,
					Value:   []byte(e.node),
					Session: session,
				}

				aquired, _, err := e.kv.KV().Acquire(pair, nil)
				if err != nil {
					log.Warn(err)
				}

				if aquired {
					log.Infof("%s is now the leader", e.node)
				}
			} else {
				log.Infof("I am the leader")
			}

			kv, _, err := e.kv.KV().Get(e.key, nil)
			if err != nil {
				log.Error(err)
			} else {
				if kv == nil {
					log.Warnf("The leader key %s does not exist", e.key)
				} else {
					if kv.Session != "" {
						log.Infof("Current leader: %s, session: %s", string(kv.Value), kv.Session)
					}
				}
			}

			time.Sleep(time.Duration(e.checkIntvl) * time.Second)
		}
	}

	log.Infof("Election loop %s exit", e.key)
}

func (e *Elector) getSession(sessionName string) string {

	sessions, _, err := e.c.Session().List(nil)
	for _, session := range sessions {
		log.Infof("session node %s, name %s, mine node: %s", session.Node, sessionName, e.node)
		if session.Name == sessionName && session.Node == e.node {
			return session.ID
		}
	}

	log.Info("No leadership sessions found, creating...")

	sessionEntry := &capi.SessionEntry{Name: sessionName, Node: e.node}
	session, _, err := e.c.Session().Create(sessionEntry, nil)
	if err != nil {
		log.Warn(err)
	}
	return session
}
