package consul

import (
	"errors"
	"fmt"

	capi "github.com/hashicorp/consul/api"
	"gitlab.com/keysaim/goinfra/log"
)

type ConsulCatalog struct {
	c    *capi.Client
	ctlg *capi.Catalog
}

func NewConsulCatalog(ctlg *capi.Catalog) *ConsulCatalog {

	return &ConsulCatalog{
		ctlg: ctlg,
	}
}

func (cc *ConsulCatalog) Catalog() *capi.Catalog {
	return cc.ctlg
}

func (cc *ConsulCatalog) GetSvcAddr(name, tag string) (string, error) {

	s, err := cc.GetSvc(name, tag)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s:%d", s.ServiceAddress, s.ServicePort), nil
}

func (cc *ConsulCatalog) GetSvc(name, tag string) (*capi.CatalogService, error) {

	sl, err := cc.GetSvcs(name, tag)
	if len(sl) == 0 {
		log.Errorf("No such service: %s with tag: %s found", name, tag)
		return nil, errors.New("Service not found")
	}
	return sl[0], err
}

func (cc *ConsulCatalog) GetSvcs(name, tag string) ([]*capi.CatalogService, error) {

	sl, _, err := cc.ctlg.Service(name, tag, nil)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	return sl, err
}
