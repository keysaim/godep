package gt03

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"

	_ "gitlab.com/keysaim/goinfra/log"
)

func copyData(data []byte, start int, bits int, length int) ([]byte, error) {
	sidx := start / 8
	eidx := (start + bits - 1) / 8
	size := eidx - sidx + 1
	if eidx >= len(data) {
		return nil, errors.New(fmt.Sprintf("Not enough data, data length: %d bytes, bits offset: %d bytes, bits size: %d bytes", len(data), sidx, size))
	}

	offset := uint32(start % 8)
	if size > length {
		length = size
	}
	adata := make([]byte, length)

	var end uint8 = data[eidx]
	// set zero to the unused bits in the last byte
	hoff := uint32(0)
	if (start+bits)%8 != 0 {
		hoff = uint32(8 - (start+bits)%8)
	}
	end = (end << hoff) >> hoff
	// Little endian
	for idx := 0; idx < size; idx += 1 {
		var v1 uint8 = data[sidx+idx]
		var val uint8

		val = v1 >> offset
		didx := sidx + idx
		if didx+1 < eidx {
			var v2 uint8 = data[didx+1]
			val |= v2 << (8 - offset)
		} else if didx+1 == eidx {
			//val |= end << (8 - offset)
			tmp := end << (8 - offset)
			val |= tmp
		} else {
			val = end >> offset
		}

		adata[idx] = byte(val)
	}

	return adata, nil
}

func BitParseUint32(data []byte, start int, bits int) (uint32, error) {
	adata, err := copyData(data, start, bits, 4)
	if err != nil {
		return 0, err
	}

	var val uint32
	buf := bytes.NewReader(adata)
	err = binary.Read(buf, binary.LittleEndian, &val)
	if err != nil {
		return 0, err
	}

	return val, nil
}

func BitParseInt32(data []byte, start int, bits int) (int32, error) {
	val, err := BitParseUint32(data, start, bits)
	if err != nil {
		return 0, err
	}
	return int32(val), nil
}
