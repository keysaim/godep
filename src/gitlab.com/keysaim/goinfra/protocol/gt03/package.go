package gt03

import (
	"encoding/binary"
	"errors"
	"fmt"
	_ "math"
	"strings"
	"time"

	"gitlab.com/keysaim/goinfra/hashx/crc16"
	"gitlab.com/keysaim/goinfra/iox/buffer"
	"gitlab.com/keysaim/goinfra/log"
)

var ERR_BAD_PKT error = errors.New("bad package")
var ERR_BAD_INFO error = errors.New("bad info")
var ERR_BAD_VERSION error = errors.New("bad version")
var ERR_BAD_STOP error = errors.New("bad stop bytes")
var ERR_PKT_INCOMPLETE error = errors.New("package not complete")

//var GT_BYTE_ORDER binary.ByteOrder = binary.LittleEndian
var GT_BYTE_ORDER binary.ByteOrder = binary.BigEndian

const (
	GT_INFO_NONE           = 0x00
	GT_INFO_LOGIN          = 0x01
	GT_INFO_GPS            = 0x10
	GT_INFO_LBS            = 0x11
	GT_INFO_STATUS         = 0x13
	GT_INFO_GPS_LBS_STATUS = 0x16
	GT_INFO_LBS_PHONE      = 0x17
	GT_INFO_EXT_LBS        = 0x18
	GT_INFO_LBS_STATUS     = 0x19
	GT_INFO_GPS_PHONE      = 0x1A
	GT_INFO_SETTING        = 0x80
	GT_INFO_QUERY          = 0x81
	GT_INFO_AGPS           = 0x1B
)

const (
	GT_WARN_NONE     = byte(0x00)
	GT_WARN_BUZZ     = byte(0x01)
	GT_WARN_LOW_VOL  = byte(0x03)
	GT_WARN_SOS      = byte(0x04)
	GT_WARN_FENCE_IN = byte(0x05)
	GT_WARN_OUT      = byte(0x06)
)

const (
	GT_WARN_EXT_NONE         = byte(0x00)
	GT_WARN_EXT_SOS          = byte(0x01)
	GT_WARN_EXT_POWER_OFF    = byte(0x02)
	GT_WARN_EXT_BUZZ         = byte(0x03)
	GT_WARN_EXT_FENCE_IN     = byte(0x04)
	GT_WARN_EXT_FENCE_OUT    = byte(0x05)
	GT_WARN_EXT_SPEED        = byte(0x06)
	GT_WARN_EXT_SHIFT        = byte(0x09)
	GT_WARN_EXT_GPS_BLIND_IN = byte(0x0A)
	GT_WARN_EXT_GPS_BIND_OUT = byte(0x0B)
	GT_WARN_EXT_POWER_ON     = byte(0x0C)
	GT_WARN_EXT_GPS_FIRST    = byte(0x0D)
)

const (
	GT_GSM_NONE      = byte(0x00)
	GT_GSM_VERY_WEAK = byte(0x01)
	GT_GSM_WEAK      = byte(0x02)
	GT_GSM_GOOD      = byte(0x03)
	GT_GSM_VERY_GOOD = byte(0x04)
)

const (
	GT_LANG_CHN = byte(0x01)
	GT_LANG_ENG = byte(0x02)
)

type Gt03Codec interface {
	Decode(*buffer.ValueBuf) (int, error)
	EncodeRsps(*buffer.ValueBuf) (int, error)
}

type Gt03Pkt struct {
	size    int16
	ctype   byte
	seq     int16
	content Gt03Codec
}

func NewPackage() *Gt03Pkt {
	return &Gt03Pkt{}
}

func (gt *Gt03Pkt) Type() byte {
	return gt.ctype
}

func (gt *Gt03Pkt) Content() Gt03Codec {
	return gt.content
}

func (gt *Gt03Pkt) Seq() int16 {
	return gt.seq
}

func (gt *Gt03Pkt) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	if data.Len() < 10 {
		log.Errorf("data len: %d < 10. ", data.Len(), ERR_PKT_INCOMPLETE)
		return lastSize - data.Len(), ERR_PKT_INCOMPLETE
	}

	s1, _ := data.ReadByte()
	s2, _ := data.ReadByte()
	if s1 != 0x78 || s2 != 0x78 {
		log.Errorf("bad start package bits %x %x", s1, s2)
		return lastSize - data.Len(), ERR_BAD_PKT
	}

	size, _ := data.ReadByte()
	gt.size = int16(size)

	if int(gt.size)+2 > data.Len() {
		log.Errorf("content len: %d > data len: %d. ", gt.size, data.Len()-2, ERR_PKT_INCOMPLETE)
		return lastSize - data.Len(), ERR_PKT_INCOMPLETE
	}

	gt.ctype, _ = data.ReadByte()

	// TODO check the CRC before further parsing

	info, err := CreateCodecByType(gt.ctype)
	if err != nil {
		return lastSize - data.Len(), err
	}
	gt.content = info

	_, err = info.Decode(data)

	if err != nil {
		return lastSize - data.Len(), err
	}

	// read the seq
	gt.seq, err = data.ReadInt16(GT_BYTE_ORDER)
	if err != nil {
		log.Error("binary.Read failed:", err)
		return lastSize - data.Len(), err
	}
	data.Next(2) // skip the CRC bytes

	// Check the stop bytes, make sure they are "0x0D 0x0A"
	t1, _ := data.ReadByte()
	t2, _ := data.ReadByte()
	if t1 != 0x0D || t2 != 0x0A {
		log.Errorf("invalid stop bytes %x %x", t1, t2)
		return lastSize - data.Len(), ERR_BAD_STOP
	}
	return lastSize - data.Len(), nil
}

func (gt *Gt03Pkt) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	if gt.IsRawRsps() {
		return gt.content.EncodeRsps(buf)
	}

	lastSize := buf.Len()
	eng := gt.IsEng()

	buf.WriteByte(0x78)
	buf.WriteByte(0x78)
	if eng {
		buf.WriteInt16(GT_BYTE_ORDER, int16(0))
	} else {
		buf.WriteByte(byte(0x00))
	}
	if gt.ctype != 0x00 {
		buf.WriteByte(gt.ctype)
	} else {
		ctype, err := GetTypeByCodec(gt.content)
		if err != nil {
			return buf.Len() - lastSize, nil
		}
		gt.ctype = ctype
		buf.WriteByte(ctype)
	}

	if gt.content != nil {
		_, err := gt.content.EncodeRsps(buf)
		if err != nil {
			return buf.Len() - lastSize, err
		}
	}
	err := buf.WriteInt16(GT_BYTE_ORDER, gt.seq)
	if err != nil {
		return buf.Len() - lastSize, err
	}

	if eng {
		buf.WriteValueAt(GT_BYTE_ORDER, int16(buf.Len()-2), 2)
	} else {
		buf.WriteValueAt(GT_BYTE_ORDER, byte(buf.Len()-1), 2)
	}

	// Write the CRC
	d, err := buf.Peek(lastSize+2, buf.Len()-lastSize)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	crc := crc16.Itu(d)
	buf.WriteUint16(GT_BYTE_ORDER, crc)

	buf.WriteByte(0x0D)
	buf.WriteByte(0x0A)
	return buf.Len() - lastSize, nil
}

func (gt *Gt03Pkt) IsEng() bool {
	switch info := gt.content.(type) {
	case *GpsLbsStatusInfo:
		return info.GetLang() == GT_LANG_ENG
	case *LbsPhoneInfo:
		return info.cmd.lang == GT_LANG_ENG
	default:
		return false
	}
}

func (gt *Gt03Pkt) IsRawRsps() bool {
	switch gt.content.(type) {
	case *AgpsInfo:
		return true
	default:
		return false
	}
}

// ======================================================================
type LoginInfo struct {
	imei     []byte
	imeiStr  string
	version  [2]byte
	verStr   string
	zoneVal  uint16
	zoneWest bool
	zone     string
}

func (lg *LoginInfo) Imei() string {
	return lg.imeiStr
}

func (lg *LoginInfo) decodeImei(imei []byte) string {
	b := make([]byte, len(imei)*2)
	for idx, c := range imei {
		bi := idx * 2
		b[bi] = c >> 4
		b[bi+1] = c & 0x0F
	}
	for i, c := range b {
		if c == byte(0) {
			b[i] = '0'
		} else {
			b[i] = byte('1') + c - byte(1)
		}
	}
	return string(b[1:])
}

func (lg *LoginInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()
	lg.imei = make([]byte, 8)
	copy(lg.imei, data.Next(8))
	lg.imeiStr = lg.decodeImei(lg.imei)

	// parse the version
	lg.version[0], _ = data.ReadByte()
	lg.version[1], _ = data.ReadByte()
	if lg.version[0] == 0x10 && lg.version[1] == 0x0A {
		lg.verStr = "GT03A"
	} else if lg.version[0] == 0x10 && lg.version[1] == 0x0B {
		lg.verStr = "GT03B"
	} else if lg.version[0] == 0x20 && lg.version[1] == 0x20 {
		lg.verStr = "GT03D"
	} else {
		log.Errorf("invalid version type %X %X", lg.version[0], lg.version[1])
		return lastSize - data.Len(), ERR_BAD_VERSION
	}

	// parse the zone
	var val uint16
	_, err := data.PeekBitValue(GT_BYTE_ORDER, &val, 4, 12)
	if err != nil {
		log.Errorf("parse zone failed")
		return lastSize - data.Len(), err
	}
	lg.zoneVal = val
	c, _ := data.ReadByte()
	t := (c & 0x04) >> 3
	data.ReadByte()
	lg.zoneWest = false
	if t == 1 {
		lg.zoneWest = true
	}
	west := "-"
	if !lg.zoneWest {
		west = "+"
	}
	hour := lg.zoneVal / 100
	minu := lg.zoneVal - 100*(lg.zoneVal/100)
	lg.zone = fmt.Sprintf("GMT%s%d:%02d", west, hour, minu)

	return lastSize - data.Len(), nil
}

func (lg *LoginInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()
	_, err := buf.Write(lg.imei)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	_, err = buf.Write(lg.version[:])
	if err != nil {
		return buf.Len() - lastSize, err
	}
	return buf.Len() - lastSize, nil
}

// ======================================================================
type GpsInfo struct {
	combined bool
	date     time.Time
	size     byte
	num      byte
	lat      float64
	lng      float64
	vel      byte
	diffPos  bool
	hasPos   bool
	latNorth bool
	lngWest  bool
	dir      int16
}

func (gi *GpsInfo) Lat() float64 {
	return gi.lat
}

func (gi *GpsInfo) Lng() float64 {
	return gi.lng
}

func (gi *GpsInfo) Vel() byte {
	return gi.vel
}

func (gi *GpsInfo) Date() time.Time {
	return gi.date
}

func (gi *GpsInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	if !gi.combined {
		date, err := ReadDate(data)
		if err != nil {
			return lastSize - data.Len(), err
		}
		gi.date = date
	}
	c, _ := data.ReadByte()
	gi.size = uint8(c) >> 4
	gi.num = uint8(c) << 4 >> 4

	val, err := data.ReadUint32(GT_BYTE_ORDER)
	if err != nil {
		return lastSize - data.Len(), err
	}
	gi.lat = float64(val) / float64(30000.0) / float64(60.0)
	val, err = data.ReadUint32(GT_BYTE_ORDER)
	if err != nil {
		return lastSize - data.Len(), err
	}
	gi.lng = float64(val) / float64(30000.0) / float64(60.0)

	gi.vel, _ = data.ReadByte()

	c, _ = data.ReadByte()
	if c&0x20 != byte(0) {
		gi.diffPos = true
	}
	if c&0x10 != byte(0) {
		gi.hasPos = true
	}
	if c&0x08 != byte(0) {
		gi.lngWest = true
	}
	if c&0x04 != byte(0) {
		gi.latNorth = true
	}
	c2, _ := data.ReadByte()
	gi.dir = int16(c&0x03)<<8 + int16(c2)

	if !gi.combined {
		data.Next(2)
	}

	return lastSize - data.Len(), nil
}

func (gi *GpsInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	return 0, nil
}

// ======================================================================
type LbsInfo struct {
	combined bool
	hasLen   bool
	isExt    bool
	date     time.Time
	mcc      int16
	mnc      byte
	lac      int16
	cell     int32
	rssi     byte
	nlac1    uint16
	nci1     uint16
	nrssi1   byte
	nlac2    uint16
	nci2     uint16
	nrssi2   byte
	nlac3    uint16
	nci3     uint16
	nrssi3   byte
	nlac4    uint16
	nci4     uint16
	nrssi4   byte
	nlac5    uint16
	nci5     uint16
	nrssi5   byte
	nlac6    uint16
	nci6     uint16
	nrssi6   byte
}

func NewExtLbsInfo() *LbsInfo {
	return &LbsInfo{
		isExt: true,
	}
}

func (li *LbsInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	if !li.combined {
		date, err := ReadDate(data)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.date = date
	} else if li.hasLen {
		_, err := data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
	}

	mcc, err := data.ReadInt16(GT_BYTE_ORDER)
	if err != nil {
		return lastSize - data.Len(), err
	}
	li.mcc = mcc
	li.mnc, err = data.ReadByte()
	if err != nil {
		return lastSize - data.Len(), err
	}
	li.lac, err = data.ReadInt16(GT_BYTE_ORDER)
	clen := 3
	if li.isExt {
		clen = 2
	}
	_, err = data.PeekBitValue(GT_BYTE_ORDER, &li.cell, 0, clen*8)
	if err != nil {
		return lastSize - data.Len(), err
	}
	data.Next(clen)

	if li.isExt {
		// Read all the extended fields
		li.rssi, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nlac1, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci1, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi1, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}

		li.nlac2, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci2, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi2, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}

		li.nlac3, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci3, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi3, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}

		li.nlac4, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci4, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi4, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}

		li.nlac5, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci5, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi5, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}

		li.nlac6, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nci6, err = data.ReadUint16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
		li.nrssi6, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
	}

	if !li.combined {
		data.Next(2)
	}

	return lastSize - data.Len(), nil
}

func (li *LbsInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	return 0, nil
}

// ======================================================================
type StatusInfo struct {
	combined bool
	buzz     bool
	fence    bool
	charge   bool
	warn     byte
	hasPos   bool
	vol      byte
	gsm      byte
	warnExt  byte
	lang     byte
	fenceNum byte
}

func (si *StatusInfo) Vol() byte {
	return si.vol
}

func (si *StatusInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	c, err := data.ReadByte()
	if err != nil {
		return lastSize - data.Len(), err
	}
	if (c & 0x01) != 0 {
		si.buzz = true
	}
	if (c & 0x02) != 0 {
		si.fence = true
	}
	if (c & 0x04) != 0 {
		si.charge = true
	}
	si.warn = byte((c & 0x38) >> 3)
	if (c & 0x40) != 0 {
		si.hasPos = true
	}

	si.vol, err = data.ReadByte()
	if err != nil {
		return lastSize - data.Len(), err
	}
	si.gsm, err = data.ReadByte()
	if err != nil {
		return lastSize - data.Len(), err
	}

	if si.combined {
		si.warnExt, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
		si.lang, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
		si.fenceNum, err = data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
	} else {
		data.Next(2)
	}

	return lastSize - data.Len(), nil
}

func (si *StatusInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	return 0, nil
}

// ======================================================================
type CmdInfo struct {
	serverId int32
	cmd      string
	lang     byte
}

func NewCmdInfo(lang byte) *CmdInfo {
	return &CmdInfo{
		lang: lang,
	}
}

func NewDefCmdInfo() *CmdInfo {
	obj := NewCmdInfo(GT_LANG_CHN)
	return obj
}

func NewDefCmdInfoWithValues(serverId int32, cmd string) *CmdInfo {
	obj := NewCmdInfo(GT_LANG_CHN)
	obj.serverId = serverId
	obj.cmd = cmd
	return obj
}

func (ci *CmdInfo) SetGpsOn() {
	ci.cmd = "GPSON#"
}

func (ci *CmdInfo) SetPhones(phones []string) {
	segs := append([]string{"SOS", "A"}, phones...)
	ci.cmd = strings.Join(segs, ",") + "#"
}

func (ci *CmdInfo) SetQueryPhones() {
	ci.cmd = "SEESOS#"
}

func (ci *CmdInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()
	var size int16

	if ci.lang == GT_LANG_ENG {
		_, err := data.ReadInt16(GT_BYTE_ORDER)
		if err != nil {
			return lastSize - data.Len(), err
		}
	} else {
		c, err := data.ReadByte()
		if err != nil {
			return lastSize - data.Len(), err
		}
		size = int16(c)
	}

	serverId, err := data.ReadInt32(GT_BYTE_ORDER)
	if err != nil {
		return lastSize - data.Len(), err
	}
	ci.serverId = serverId

	cd := data.Next(int(size) - 4)
	if cd == nil {
		return lastSize - data.Len(), err
	}

	ci.cmd = string(cd)
	return lastSize - data.Len(), nil
}

func (ci *CmdInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()

	if ci.lang == GT_LANG_CHN {
		// For Chinese cmd
		err := buf.WriteByte(byte(0x00))
		if err != nil {
			return buf.Len() - lastSize, err
		}
	} else if ci.lang == GT_LANG_ENG {
		err := buf.WriteInt16(GT_BYTE_ORDER, int16(0))
		if err != nil {
			return buf.Len() - lastSize, err
		}
	} else {
		return 0, ERR_BAD_PKT
	}

	err := buf.WriteInt32(GT_BYTE_ORDER, ci.serverId)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	_, err = buf.WriteString(ci.cmd)
	if err != nil {
		return buf.Len() - lastSize, err
	}

	if ci.lang == GT_LANG_CHN {
		err = buf.WriteValueAt(GT_BYTE_ORDER, byte(buf.Len()-lastSize-1), int64(lastSize))
	} else {
		err = buf.WriteValueAt(GT_BYTE_ORDER, int16(buf.Len()-lastSize-2), int64(lastSize))
	}
	return buf.Len() - lastSize, nil
}

// ======================================================================
type GpsLbsStatusInfo struct {
	date   time.Time
	gps    GpsInfo
	lbs    LbsInfo
	status StatusInfo
	cmd    CmdInfo
}

func NewGpsLbsStatusInfo() *GpsLbsStatusInfo {
	return &GpsLbsStatusInfo{
		gps:    GpsInfo{combined: true},
		lbs:    LbsInfo{combined: true, hasLen: true},
		status: StatusInfo{combined: true},
	}
}

func (ci *GpsLbsStatusInfo) Status() *StatusInfo {
	return &ci.status
}

func (ci *GpsLbsStatusInfo) GetLang() byte {
	return ci.status.lang
}

func (ci *GpsLbsStatusInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	date, err := ReadDate(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	ci.date = date

	_, err = ci.gps.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	_, err = ci.lbs.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	_, err = ci.status.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	return lastSize - data.Len(), nil
}

func (ci *GpsLbsStatusInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()

	_, err := ci.cmd.EncodeRsps(buf)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	return buf.Len() - lastSize, nil
}

// ======================================================================
type LbsPhoneInfo struct {
	lbs   LbsInfo
	phone []byte
	cmd   CmdInfo
}

func NewLbsPhoneInfo() *LbsPhoneInfo {
	return &LbsPhoneInfo{
		lbs: LbsInfo{combined: true, hasLen: false},
	}
}

func (lp *LbsPhoneInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	_, err := lp.lbs.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	lp.phone = data.Next(21)

	return lastSize - data.Len(), nil
}

func (lp *LbsPhoneInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()

	_, err := lp.cmd.EncodeRsps(buf)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	return buf.Len() - lastSize, nil
}

// ======================================================================
type LbsStatusInfo struct {
	lbs    LbsInfo
	status StatusInfo
}

func NewLbsStatusInfo() *LbsStatusInfo {
	return &LbsStatusInfo{
		lbs:    LbsInfo{combined: true, hasLen: false},
		status: StatusInfo{combined: true},
	}
}

func (ls *LbsStatusInfo) Status() *StatusInfo {
	return &ls.status
}

func (ls *LbsStatusInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	_, err := ls.lbs.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}

	_, err = ls.status.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	return lastSize - data.Len(), nil
}

func (lp *LbsStatusInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	return 0, nil
}

// ======================================================================
type GpsPhoneInfo struct {
	date  time.Time
	gps   GpsInfo
	phone []byte
	cmd   CmdInfo
}

func NewGpsPhoneInfo() *GpsPhoneInfo {
	return &GpsPhoneInfo{
		gps: GpsInfo{combined: true},
	}
}

func (gp *GpsPhoneInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	d, err := ReadDate(data)
	if err != nil {
		return lastSize - data.Len(), err
	}
	gp.date = d

	_, err = gp.gps.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}

	gp.phone = data.Next(21)
	if gp.phone == nil {
		return lastSize - data.Len(), err
	}
	return lastSize - data.Len(), nil
}

func (gp *GpsPhoneInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()

	_, err := gp.cmd.EncodeRsps(buf)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	return 0, nil
}

// ======================================================================
type SettingInfo struct {
	*CmdInfo
}

func NewSettingInfo() *SettingInfo {
	return &SettingInfo{
		CmdInfo: NewDefCmdInfo(),
	}
}

type QueryInfo struct {
	*CmdInfo
}

func NewQueryInfo() *QueryInfo {
	return &QueryInfo{
		CmdInfo: NewDefCmdInfo(),
	}
}

// ======================================================================
type AgpsInfo struct {
	lbs  LbsInfo
	info string
}

func NewAgpsInfo() *AgpsInfo {
	return &AgpsInfo{
		lbs: LbsInfo{combined: true, hasLen: false},
	}
}

func (ai *AgpsInfo) Decode(data *buffer.ValueBuf) (int, error) {
	lastSize := data.Len()

	_, err := ai.lbs.Decode(data)
	if err != nil {
		return lastSize - data.Len(), err
	}

	return lastSize - data.Len(), nil
}

func (ai *AgpsInfo) EncodeRsps(buf *buffer.ValueBuf) (int, error) {
	lastSize := buf.Len()

	buf.WriteByte(byte(0x78))
	buf.WriteByte(byte(0x78))
	buf.WriteByte(byte(0x1B))
	buf.WriteByte(byte(0))
	buf.WriteByte(byte(0))
	buf.WriteString(ai.info)

	err := buf.WriteValueAt(GT_BYTE_ORDER, int16(buf.Len()-lastSize-5), int64(lastSize+3))
	if err != nil {
		return buf.Len() - lastSize, err
	}

	d, err := buf.Peek(lastSize+2, buf.Len()-lastSize)
	if err != nil {
		return buf.Len() - lastSize, err
	}
	crc := crc16.Itu(d)
	buf.WriteUint16(GT_BYTE_ORDER, crc)

	buf.WriteByte(byte(0x0D))
	buf.WriteByte(byte(0x0A))
	return buf.Len() - lastSize, nil
}

// ======================================================================
func ReadDate(data *buffer.ValueBuf) (time.Time, error) {
	if data.Len() < 6 {
		return time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC), ERR_PKT_INCOMPLETE
	}
	d := data.Next(6)
	date := time.Date(2000+int(d[0]), time.Month(d[1]), int(d[2]),
		int(d[3]), int(d[4]), int(d[5]), 0, time.UTC)

	return date, nil
}

func CreateCodecByType(ctype byte) (Gt03Codec, error) {
	switch ctype {
	case GT_INFO_LOGIN:
		return new(LoginInfo), nil
	case GT_INFO_GPS:
		return new(GpsInfo), nil
	case GT_INFO_LBS:
		return new(LbsInfo), nil
	case GT_INFO_STATUS:
		return new(StatusInfo), nil
	case GT_INFO_GPS_LBS_STATUS:
		return NewGpsLbsStatusInfo(), nil
	case GT_INFO_LBS_PHONE:
		return NewLbsPhoneInfo(), nil
	case GT_INFO_EXT_LBS:
		return NewExtLbsInfo(), nil
	case GT_INFO_LBS_STATUS:
		return NewLbsStatusInfo(), nil
	case GT_INFO_GPS_PHONE:
		return NewGpsPhoneInfo(), nil
	case GT_INFO_SETTING:
		return NewSettingInfo(), nil
	case GT_INFO_QUERY:
		return NewQueryInfo(), nil
	case GT_INFO_AGPS:
		return NewAgpsInfo(), nil
	default:
		log.Errorf("Invalid content type: %x", ctype)
		return nil, errors.New("not support content type")
	}
}

func GetTypeByCodec(c Gt03Codec) (byte, error) {
	switch info := c.(type) {
	case *LoginInfo:
		return GT_INFO_LOGIN, nil
	case *GpsInfo:
		return GT_INFO_GPS, nil
	case *LbsInfo:
		if info.isExt {
			return GT_INFO_EXT_LBS, nil
		} else {
			return GT_INFO_LBS, nil
		}
	case *StatusInfo:
		return GT_INFO_STATUS, nil
	case *GpsLbsStatusInfo:
		return GT_INFO_LBS_STATUS, nil
	case *LbsPhoneInfo:
		return GT_INFO_LBS_PHONE, nil
	case *LbsStatusInfo:
		return GT_INFO_LBS_STATUS, nil
	case *SettingInfo:
		return GT_INFO_SETTING, nil
	case *QueryInfo:
		return GT_INFO_QUERY, nil
	case *AgpsInfo:
		return GT_INFO_AGPS, nil
	default:
		return GT_INFO_NONE, ERR_BAD_INFO
	}
}
