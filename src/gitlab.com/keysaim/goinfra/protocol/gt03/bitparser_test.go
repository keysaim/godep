package gt03

import (
	"testing"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type BitParserTestSuite struct {
	suite.Suite
}

func (ts *BitParserTestSuite) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")
}

func (ts *BitParserTestSuite) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *BitParserTestSuite) TestAligned() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	data := make([]byte, 2)
	data[0] = 0xD8
	data[1] = 0x4D

	val, err := BitParseUint32(data, 4, 12)
	assert.Nil(err)
	assert.Equal(val, uint32(1245))
}

func (ts *BitParserTestSuite) TestOdd() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	data := make([]byte, 2)

	data[0] = 0x68
	data[1] = 0x68

	val, err := BitParseUint32(data, 3, 13)
	assert.Nil(err)
	assert.Equal(val, uint32(0x0D0D))
}

func (ts *BitParserTestSuite) TestMultipleBytes() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	data := make([]byte, 8)

	for idx, _ := range data {
		data[idx] = 0x68
	}

	val, err := BitParseUint32(data, 3, 25)
	assert.Nil(err)
	assert.Equal(val, uint32(0x10D0D0D))
}

func TestBitParser(t *testing.T) {
	suite.Run(t, new(BitParserTestSuite))
}
