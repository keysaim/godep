package typex

import (
	"strconv"

	"gitlab.com/keysaim/goinfra/log"
)

type JsonMap map[string]interface{}

func NewJsonMap(m map[string]string) JsonMap {

	jm := JsonMap{}
	for k, v := range m {
		jm[k] = v
	}
	return jm
}

func (m JsonMap) Update(om JsonMap) {

	for k, v := range om {
		m[k] = v
	}
}

func (m JsonMap) GetString(name string) string {
	if v, ok := m[name]; ok {
		if s, ok := v.(string); ok {
			return s
		}
	}
	return ""
}

func (m JsonMap) GetStringX(name, def string) string {
	if v, ok := m[name]; ok {
		if s, ok := v.(string); ok {
			return s
		}
	}
	return def
}

func (m JsonMap) GetStrings(name string) []string {
	if v, ok := m[name]; ok {
		if s, ok := v.([]string); ok {
			return s
		}
	}
	return nil
}

func (m JsonMap) GetIfaceStrings(name string) []string {

	if v, ok := m[name]; ok {
		if vl, ok := v.([]interface{}); ok {
			var sl []string
			for _, iv := range vl {
				if s, ok := iv.(string); ok {
					sl = append(sl, s)
				} else {
					return nil
				}
			}
			return sl
		}
	}
	return nil
}

func (m JsonMap) GetBool(name string) bool {
	if v, ok := m[name]; ok {
		if s, ok := v.(bool); ok {
			return s
		}
	}
	return false
}

func (m JsonMap) GetBoolX(name string, def bool) bool {
	if v, ok := m[name]; ok {
		if s, ok := v.(bool); ok {
			return s
		}
	}
	return def
}

func (m JsonMap) GetInt(name string) int {
	return int(m.GetInt64(name))
}

func (m JsonMap) GetIntX(name string, def int) int {

	if _, ok := m[name]; !ok {
		return def
	}

	return int(m.GetInt64(name))
}

func (m JsonMap) GetUint32(name string) uint32 {
	return uint32(m.GetInt64(name))
}

func (m JsonMap) GetInt64(name string) int64 {
	if v, ok := m[name]; ok {
		switch cv := v.(type) {
		case float32:
			return int64(cv)
		case float64:
			return int64(cv)
		case int64:
			return cv
		case int:
			return int64(cv)
		case string:
			i, err := strconv.ParseInt(cv, 10, 64)
			if err == nil {
				return i
			} else {
				log.Errorf("Cannot convert field: %s from string to int64, with error: %s", name, err.Error())
			}
		default:
			log.Errorf("GetInt64 failed for field: %s, unsupported value type: %T", name, v)
		}
	}
	return 0
}

func (m JsonMap) GetObject(name string) JsonMap {
	if v, ok := m[name]; ok {
		if s, ok := v.(map[string]interface{}); ok {
			return JsonMap(s)
		}
	}

	return nil
}

func (m JsonMap) GetStringMap(name string) map[string]string {

	jm := m.GetObject(name)
	if len(jm) == 0 {
		return nil
	}

	sm := map[string]string{}
	for k, v := range jm {
		if s, ok := v.(string); ok {
			sm[k] = s
		}
	}
	return sm
}
