package tcp

import (
	"bufio"
	"fmt"
	"net"
	"sync"
	"testing"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type TcpServerTestSuite struct {
	suite.Suite
}

func (ts *TcpServerTestSuite) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")
}

func (ts *TcpServerTestSuite) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *TcpServerTestSuite) TestReadWrite() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	port := 22222
	server := NewTcpServer(port)
	connChan := make(chan *TcpConn)
	sendMsg := "Send Hello World!\n"

	err := server.Start(connChan)
	assert.Nil(err)
	exitCg := &sync.WaitGroup{}

	exitCg.Add(1)
	go func() {
		defer func() {
			exitCg.Done()
		}()

		select {
		case conn := <-connChan:
			log.Infof("Got one connection %#v", conn)

			ec := make(chan error)
			conn.Start(func(data []byte) error {
				msg := string(data)
				log.Infof("Server received one msg: %s", msg)
				assert.Equal(msg, sendMsg)
				log.Infof("Server sends msg: %s", sendMsg)
				conn.Send([]byte(sendMsg))
				go func() {
					conn.Stop()
				}()
				return nil
			}, ec)

			err := <-ec
			log.Infof("Connection exit with err: %s", err)
		}
	}()

	addr := fmt.Sprintf("127.0.0.1:%d", port)
	log.Infof("listen addr: %s", addr)
	conn, err := net.Dial("tcp", addr)
	assert.Nil(err)

	log.Infof("Client sends msg: %s", sendMsg)
	fmt.Fprintf(conn, sendMsg)

	msg, _ := bufio.NewReader(conn).ReadString('\n')
	log.Infof("Client received msg: %s", msg)
	assert.Equal(msg, sendMsg)

	exitCg.Wait()
	server.Stop()
}

func TestTcpServer(t *testing.T) {
	suite.Run(t, new(TcpServerTestSuite))
}
