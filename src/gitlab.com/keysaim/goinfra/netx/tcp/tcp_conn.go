package tcp

import (
	"io"
	"net"
	"strings"
	"sync"

	"gitlab.com/keysaim/goinfra/log"
)

type TcpConn struct {
	conn   net.Conn
	status error
	exitCg *sync.WaitGroup
}

func NewTcpConn(conn net.Conn) *TcpConn {
	return &TcpConn{
		conn:   conn,
		exitCg: &sync.WaitGroup{},
	}
}

func (tc *TcpConn) Start(onRead func([]byte) error, exitChan chan<- error) {
	buf := make([]byte, 1024)
	tc.exitCg.Add(1)
	go func() {
		defer func() {
			tc.conn.Close()
			tc.exitCg.Done()
			exitChan <- tc.status
		}()

		for {
			size, err := tc.conn.Read(buf)
			if err != nil {
				if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
					log.Infof("Temporary error when accepting new connections: %s", netErr)
					continue
				}

				if err != io.EOF && !strings.Contains(err.Error(), "use of closed network connection") {
					log.Error("Tcp Connection read err:", err)
				} else {
					log.Info("Tcp connection is closed")
				}

				tc.status = err
				return
			}

			if err := onRead(buf[:size]); err != nil {
				log.Error("Tcp Connection onRead error:", err)
				tc.status = err
				return
			}
		}
	}()
}

func (tc *TcpConn) Stop() {
	tc.conn.Close()
	tc.exitCg.Wait()
	log.Info("Tcp Connection stopped")
}

func (tc *TcpConn) Send(data []byte) {
	for off := 0; off < len(data); {
		n, err := tc.conn.Write(data[off:])
		if err != nil {
			log.Error("Tcp Connection write error:", err)
			tc.status = err
			return
		}
		off += n
	}
}
