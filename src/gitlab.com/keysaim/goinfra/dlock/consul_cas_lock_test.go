package dlock

import (
	"fmt"
	"testing"
	"time"

	capi "github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func TestConsulCasDlock(t *testing.T) {

	log.SetDefLevelStr("debug")
	require := require.New(t)
	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	key := "test/lock1"
	lock1 := NewConsulCasDlock(c, key, 10, "")
	lock1.Destroy()
	owner, err := lock1.Lock()
	require.Nil(err)
	require.NotEmpty(owner)
	err = lock1.Unlock()
	require.Nil(err)
	kvp, _, err := c.KV().Get(key, nil)
	require.Nil(err)
	require.Nil(kvp)

	owner, err = lock1.Lock()
	require.Nil(err)
	require.NotEmpty(owner)
	err = lock1.Unlock()
	require.Nil(err)
	o, err := lock1.getOwner()
	require.Nil(err)
	require.Empty(o)

	owner, err = lock1.Lock()
	require.Nil(err)
	require.NotEmpty(owner)
	nowner, err := lock1.Lock()
	require.Nil(err)
	require.NotEmpty(nowner)

	lock2 := NewConsulCasDlock(c, key, 10, "")
	owner2, err := lock2.Lock()
	require.NotNil(err)
	require.Equal(lock1.owner, owner2)

	err = lock1.Unlock()
	require.Nil(err)
	o, err = lock1.Relock()
	require.Nil(err)
	require.Equal(lock1.owner, o)
	o, err = lock1.getOwner()
	require.Nil(err)
	require.Equal(lock1.owner, o)

	lock2 = NewConsulCasDlock(c, key, 10, lock1.owner)
	o, err = lock2.Lock()
	require.Nil(err)
	require.Equal(lock1.owner, o)
	require.Equal(lock2.owner, o)
	o, err = lock2.Relock()
	require.Nil(err)
	require.Equal(lock2.owner, o)
	err = lock2.Unlock()
	require.Nil(err)

	lock1 = NewConsulCasDlock(c, key, 1, "")
	owner, err = lock1.Lock()
	require.Nil(err)
	require.NotEmpty(owner)
	lock2 = NewConsulCasDlock(c, key, 1, "")
	o, err = lock2.Lock()
	require.NotNil(err)
	require.Equal(owner, o)
	time.Sleep(1500 * time.Millisecond)
	lock2 = NewConsulCasDlock(c, key, 1, "")
	o, err = lock2.Lock()
	require.Nil(err)
	require.NotEqual(owner, o)
}

func consulCasLock(c *capi.Client, n int) {

	ll := make([]*ConsulCasDlock, n)
	for i := range ll {
		key := fmt.Sprintf("test/lock%d", i)
		lock := NewConsulCasDlock(c, key, 10, "")
		ll[i] = lock
	}

	for i := range ll {
		ll[i].Unlock()
	}
}

func BenchmarkConsulCasLock(b *testing.B) {

	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	if err != nil {
		log.Fatal(err)
	}
	for n := 0; n < b.N; n += 1 {
		consulCasLock(c, 1000)
	}
}
