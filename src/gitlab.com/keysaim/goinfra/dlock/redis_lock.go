package dlock

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/utils"
)

type RedisDlock struct {
	rl       *RedisLock
	key      string
	timeout  int
	setOwner string

	owner string
}

func NewRedisDlock(c *redis.Client, key string, timeout int, owner string) *RedisDlock {

	return &RedisDlock{
		rl:       NewRedisLock(c),
		key:      key,
		timeout:  timeout,
		setOwner: owner,
	}
}

func NewCacheRedisDlock(c *redis.Client, key string, timeout int, owner string) *RedisDlock {

	return &RedisDlock{
		rl:       NewCacheRedisLock(c),
		key:      key,
		timeout:  timeout,
		setOwner: owner,
	}
}

func (l *RedisDlock) Key() string {
	return l.key
}

func (l *RedisDlock) SetOwner(owner string) {
	l.setOwner = owner
}

func (l *RedisDlock) SetFreeze() {
	l.rl.freeze = true
}

func (l *RedisDlock) Timeout() int {
	return l.timeout
}

func (l *RedisDlock) Lock() (string, error) {

	owner, err := l.tryLock()
	if err != nil {
		if err != ErrLockIsHold {
			return owner, err
		}
		if l.owner == "" || owner != l.owner {
			return owner, err
		}
	}

	l.owner = owner
	return owner, nil
}

func (l *RedisDlock) tryLock() (string, error) {

	if l.setOwner != "" {
		return l.rl.LockWith(l.key, l.timeout, l.setOwner)
	}

	return l.rl.Lock(l.key, l.timeout)
}

func (l *RedisDlock) Relock() (string, error) {

	if l.owner == "" {
		return l.Lock()
	}

	owner, err := l.rl.Relock(l.owner, l.key, l.timeout)
	if err != nil {
		return owner, err
	}

	return owner, nil
}

func (l *RedisDlock) Unlock() error {

	if l.owner == "" {
		return nil
	}

	return l.rl.Unlock(l.owner, l.key)
}

func (l *RedisDlock) Owner() (string, error) {

	return l.rl.Owner(l.key)
}

type RedisLock struct {
	redis    *redis.Client
	hostname string
	isCache  bool
	freeze   bool
}

func NewRedisLock(redis *redis.Client) *RedisLock {

	h, err := os.Hostname()
	if err != nil {
		log.Errorf("Failed to get the hostname, error: %v", err)
	}
	return &RedisLock{
		redis:    redis,
		hostname: h,
	}
}

func NewCacheRedisLock(redis *redis.Client) *RedisLock {

	h, err := os.Hostname()
	if err != nil {
		log.Errorf("Failed to get the hostname, error: %v", err)
	}
	return &RedisLock{
		redis:    redis,
		hostname: h,
		isCache:  true,
	}
}

func (ss *RedisLock) genOwner() string {
	return fmt.Sprintf("%s-%d-%s", ss.hostname, time.Now().UnixNano(), utils.RandString(5))
}

func (ss *RedisLock) FreezeTime(key string) int64 {

	now := time.Now().Unix()
	s, _ := ss.redis.Get(key + "/lockTime").Result()
	if s == "" {
		return now
	}
	v, _ := strconv.ParseInt(s, 10, 64)
	return time.Now().Unix() - v
}

func (ss *RedisLock) LockWith(key string, timeout int, owner string) (string, error) {

	if ss.freeze {
		if ss.FreezeTime(key) < int64(timeout) {
			return "", ErrLockIsFrozen
		}
	}
	ok, err := ss.redis.SetNX(key, owner, time.Duration(timeout)*time.Second).Result()
	if err != nil {
		log.Errorf("Failed to set the lock %s, timeout: %d, error: %v", key, timeout, err)
		owner, err := ss.redis.Get(key).Result()
		return owner, err
	}
	if !ok {
		nowner, err := ss.redis.Get(key).Result()
		if err != nil {
			log.Warnf("Failed to get the key %s owner, error: %v", key, err)
		}
		if nowner == owner {
			return ss.Relock(owner, key, timeout)
		}
		return nowner, ErrLockIsHold
	}
	if ss.freeze {
		now := time.Now().Unix()
		err = ss.redis.Set(key+"/lockTime", strconv.FormatInt(now, 10), time.Duration(timeout)*time.Second).Err()
		if err != nil {
			log.Errorf("Failed to freeze the lock %s with timeout %d seconds, error: %v", key, timeout, err)
		}
	}
	return owner, nil
}

func (ss *RedisLock) Lock(key string, timeout int) (string, error) {

	owner := ss.genOwner()
	return ss.LockWith(key, timeout, owner)
}

func (ss *RedisLock) Relock(owner, key string, timeout int) (string, error) {

	if owner == "" {
		owner = ss.genOwner()
	}
	lua := "relock"
	if !ss.isCache {
		lua = "local val=redis.call(\"get\", KEYS[1]); if val == false then redis.call(\"set\", KEYS[1], ARGV[1], \"ex\", ARGV[2]); return ARGV[1] elseif val == ARGV[1] then redis.call(\"expire\", KEYS[1], ARGV[2]) end; return val"
	}
	v, err := ss.redis.Eval(lua, []string{key}, owner, timeout).Result()
	if err != nil {
		log.Errorf("Failed to relock the key %s, error: %v", key, err)
		return "", err
	}
	co := v.(string)
	if co != owner {
		log.Warnf("The lock %s is owned by %s, cannot relock", key, co)
		return co, ErrLockIsHold
	}
	return owner, nil
}

func (ss *RedisLock) Unlock(owner, key string) error {

	lua := "unlock"
	if !ss.isCache {
		lua = "local val=redis.call(\"get\", KEYS[1]); if val == ARGV[1] then redis.call(\"del\", KEYS[1]) end; return val"
	}
	v, err := ss.redis.Eval(lua, []string{key}, owner).Result()
	if err != nil {
		if err == redis.Nil {
			return nil
		}
		log.Errorf("Failed to unlock the key %s, error: %v", key, err)
		return err
	}
	co := v.(string)
	if co != owner {
		log.Warnf("The lock %s is owned by %s, cannot unlock by %s", key, co, owner)
		return ErrLockIsHold
	}
	return nil
}

func (ss *RedisLock) IsLocked(key string) bool {

	num, _ := ss.redis.Exists(key).Result()
	return num == 1
}

func (ss *RedisLock) Owner(key string) (string, error) {

	co, err := ss.redis.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}
		log.Errorf("Failed to get the lock %s, error: %v", key, err)
		return "", err
	}

	return co, nil
}
