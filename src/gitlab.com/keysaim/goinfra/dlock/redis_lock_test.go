package dlock

import (
	"fmt"
	"testing"
	"time"

	"github.com/go-redis/redis"
	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func init() {
	log.EnableLogRunTime(true)
}

func TestRedisLock(t *testing.T) {

	log.SetDefLevelStr("debug")
	require := require.New(t)
	c := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	lock := NewRedisLock(c)
	key := "test/lock1"
	c.Del(key)
	err := lock.Unlock("test", key)
	require.Nil(err)
	owner, err := lock.Lock(key, 1)
	require.Nil(err)
	require.True(owner != "")
	require.True(lock.IsLocked(key))
	o2, err := lock.Lock(key, 1)
	require.Equal(ErrLockIsHold, err)
	require.Equal(owner, o2)
	o2, err = lock.Relock(owner, key, 1)
	require.Nil(err)
	require.Equal(owner, o2)
	require.True(lock.IsLocked(key))
	time.Sleep(1200 * time.Millisecond)
	require.False(lock.IsLocked(key))
	o2, err = lock.Relock("", key, 1)
	require.Nil(err)
	require.False(o2 == "")
	require.NotEqual(owner, o2)
	err = lock.Unlock("fake", key)
	require.NotNil(err)
	require.True(lock.IsLocked(key))
	err = lock.Unlock(o2, key)
	require.Nil(err)
	require.False(lock.IsLocked(key))
}

func TestRedisDlock(t *testing.T) {
	require := require.New(t)
	c := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	key := "test/lock1"
	c.Del(key)
	lock := NewRedisDlock(c, key, 1, "")
	owner, err := lock.Lock()
	require.Nil(err)
	require.True(owner != "")
	o, err := lock.Owner()
	require.Nil(err)
	require.Equal(owner, o)
	o2, err := lock.Lock()
	require.Nil(err)
	require.Equal(owner, o2)
	o2, err = lock.Relock()
	require.Nil(err)
	require.Equal(owner, o2)
	time.Sleep(1200 * time.Millisecond)
	o, err = lock.Owner()
	require.Nil(err)
	require.Empty(o)
	o2, err = lock.Relock()
	require.Nil(err)
	require.Equal(o2, owner)
	err = lock.Unlock()
	require.Nil(err)
	o, err = lock.Owner()
	require.Nil(err)
	require.Empty(o)
	err = lock.Unlock()
	require.Nil(err)
	o, err = lock.Owner()
	require.Nil(err)
	require.Empty(o)
	// test freeze
	lock.SetFreeze()
	_, err = lock.Lock()
	require.Nil(err)
	err = lock.Unlock()
	require.Nil(err)
	_, err = lock.Lock()
	require.Equal(ErrLockIsFrozen, err)
	time.Sleep(1200 * time.Millisecond)
	_, err = lock.Lock()
	require.Nil(err)
	err = lock.Unlock()
	require.Nil(err)
	lock2 := NewRedisDlock(c, key, 1, "")
	_, err = lock2.Lock()
	require.Nil(err)
	err = lock2.Unlock()
	require.Nil(err)
}

func redisLock(c *redis.Client, n int) {

	ll := make([]*RedisDlock, n)
	for i := range ll {
		key := fmt.Sprintf("test/lock%d", i)
		lock := NewRedisDlock(c, key, 1, "")
		ll[i] = lock
	}

	for i := range ll {
		ll[i].Unlock()
	}
}

func BenchmarkRedisDlock(b *testing.B) {

	c := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	for n := 0; n < b.N; n += 1 {
		redisLock(c, 1000)
	}
}
