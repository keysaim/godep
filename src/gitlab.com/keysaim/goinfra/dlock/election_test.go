package dlock

import (
	"context"
	"sync"
	"testing"

	capi "github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func TestConsulCasElector(t *testing.T) {

	require := require.New(t)
	ctx, cancel := context.WithCancel(context.Background())
	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	require.Nil(err)
	lock1 := NewConsulCasDlock(c, "test/elect", 1, "")
	lock2 := NewConsulCasDlock(c, "test/elect", 1, "")
	le1 := NewLeaderElector(ctx, "test", lock1, 0)
	le2 := NewLeaderElector(ctx, "test", lock2, 0)

	lm := map[string]int{
		lock1.Key(): 0,
		lock2.Key(): 0,
	}
	wg := &sync.WaitGroup{}
	wg.Add(2)
	onChange := func(le *LeaderElector, e *ElectEvent) {
		require.NotEqual(ES_ERROR, e.State)
		if e.State == ES_EXIT {
			wg.Done()
			return
		}
		if e.State == ES_LEADER {
			if le == le1 {
				lm[lock1.Key()] += 1
			} else {
				lm[lock2.Key()] += 1
			}
			le.Stop()
		}
	}
	le1.Sub(onChange)
	le2.Sub(onChange)
	le1.Start()
	le2.Start()

	wg.Wait()
	log.Infof("lm: %#v", lm)
	for _, cnt := range lm {
		require.True(cnt > 0)
	}
	cancel()
}
