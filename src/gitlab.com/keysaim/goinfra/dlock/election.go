package dlock

import (
	"context"
	"time"

	"gitlab.com/keysaim/goinfra/log"
)

type ElectState int

const (
	ES_INIT ElectState = iota
	ES_LEADER
	ES_COND
	ES_EXIT
	ES_ERROR
)

func (es ElectState) String() string {

	switch es {
	case ES_INIT:
		return "ES_INIT"
	case ES_LEADER:
		return "ES_LEADER"
	case ES_COND:
		return "ES_COND"
	case ES_EXIT:
		return "ES_EXIT"
	case ES_ERROR:
		return "ES_ERROR"
	default:
		return "unknown"
	}

}

const (
	err_itvl  = 10
	cond_itvl = 2
)

type ElectEvent struct {
	LastState ElectState
	State     ElectState
	Leader    string
}

type OnStateChangeFunc func(le *LeaderElector, e *ElectEvent)

type LeaderElector struct {
	ctx       context.Context
	cancel    context.CancelFunc
	name      string
	lock      Dlock
	timeout   int
	checkItvl int

	subList []OnStateChangeFunc
	state   ElectState
	leader  string
}

func NewLeaderElector(pctx context.Context, name string, lock Dlock, checkItvl int) *LeaderElector {

	log.Infof("New leader elector %s, timeout: %d", name, lock.Timeout())
	if checkItvl <= 0 {
		checkItvl = lock.Timeout() / 2
		if checkItvl == 0 {
			checkItvl = 1
		}
	}
	e := &LeaderElector{
		name:      name,
		lock:      lock,
		timeout:   lock.Timeout(),
		checkItvl: checkItvl,
		state:     ES_INIT,
	}
	e.ctx, e.cancel = context.WithCancel(pctx)

	return e
}

func (e *LeaderElector) Name() string {
	return e.name
}

func (e *LeaderElector) Sub(f OnStateChangeFunc) {

	e.subList = append(e.subList, f)
}

func (e *LeaderElector) Start() {

	log.Infof("Start leader elector %s , timeout: %d, total subs: %d",
		e.name, e.timeout, len(e.subList))
	if len(e.subList) == 0 {
		log.Warnf("No subscriber for this leader elector %s", e.name)
	}
	go e.electLoop()
}

func (e *LeaderElector) Stop() {
	e.cancel()
}

func (e *LeaderElector) electLoop() {

	for e.ctx.Err() == nil {
		n := e.elect()
		for i := 0; i < n; i += 1 {
			if e.ctx.Err() != nil {
				break
			}
			time.Sleep(1 * time.Second)
		}
	}
	log.Infof("The leader elector %s exited", e.name)
	owner, _ := e.lock.Owner()
	e.changeState(ES_EXIT, owner)
}

func (e *LeaderElector) elect() int {

	var sleep int
	switch e.state {
	case ES_INIT:
		owner, err := e.lock.Lock()
		e.leader = owner
		if err != nil {
			if err == ErrLockIsHold {
				e.changeState(ES_COND, owner)
				sleep = cond_itvl
			} else {
				log.Error(err)
				e.changeState(ES_ERROR, owner)
				sleep = err_itvl
			}
		} else {
			e.changeState(ES_LEADER, owner)
			sleep = e.checkItvl
		}
	case ES_ERROR:
		owner, err := e.lock.Owner()
		e.leader = owner
		if err == nil {
			e.changeState(ES_INIT, owner)
		} else {
			sleep = err_itvl
		}
	case ES_LEADER:
		owner, err := e.lock.Relock()
		e.leader = owner
		if err != nil {
			if err == ErrLockIsHold {
				e.changeState(ES_COND, owner)
				sleep = cond_itvl
			} else {
				log.Error(err)
				e.changeState(ES_ERROR, owner)
				sleep = err_itvl
			}
		} else {
			sleep = e.checkItvl
		}
	case ES_COND:
		owner, err := e.lock.Lock()
		if err != nil {
			if err == ErrLockIsHold {
				if owner != e.leader {
					log.Infof("The elector %s leader changed from %s to %s", e.name, e.leader, owner)
					e.changeState(ES_COND, owner)
				}
				e.leader = owner
				sleep = cond_itvl
			} else {
				e.leader = owner
				e.changeState(ES_ERROR, owner)
				sleep = err_itvl
			}
		} else {
			e.leader = owner
			e.changeState(ES_LEADER, owner)
			sleep = e.checkItvl
		}
	}

	return sleep
}

func (e *LeaderElector) changeState(state ElectState, owner string) {

	if state == ES_LEADER {
		log.Infof("The elector %s became leader", e.name)
	} else if e.state == ES_LEADER {
		log.Infof("The elector %s lost leader", e.name)
	}
	log.Infof("Change elector %s state from %s to %s, leader: %s",
		e.name, e.state, state, owner)

	lastState := state
	e.state = state

	e.notify(lastState, state, owner)
}

func (e *LeaderElector) notify(lastState, state ElectState, owner string) {

	event := &ElectEvent{
		LastState: lastState,
		State:     state,
		Leader:    owner,
	}
	for _, f := range e.subList {
		f(e, event)
	}
}
