package dlock

import (
	"errors"
)

var ErrLockIsHold error = errors.New("lock is hold")
var ErrLockIsFrozen error = errors.New("lock is frozen")

type Dlock interface {
	Key() string
	Lock() (string, error)
	Relock() (string, error)
	Unlock() error
	SetOwner(owner string)
	Owner() (string, error)
	Timeout() int
	SetFreeze()
}

type NoDlock struct {
}

func (l *NoDlock) Lock() (string, error) {
	return "", nil
}

func (l *NoDlock) Relock() (string, error) {
	return "", nil
}

func (l *NoDlock) Unlock() error {
	return nil
}

func (l *NoDlock) SetOwner(owner string) {
}

func (l *NoDlock) Owner() (string, error) {
	return "", nil
}

func (l *NoDlock) Timeout() int {
	return 0
}

func (l *NoDlock) SetFreeze() {
}
