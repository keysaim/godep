package ctx

import (
	"github.com/spf13/viper"
	"gitlab.com/keysaim/goinfra/log"
)

type Service interface {
	Name() string
	CC() ContextChannel
	Cfg() *viper.Viper
	OnCfgChange()
	Start() error
	Stop() error
}

type BaseService struct {
	name string
	cc   ContextChannel
	v    *viper.Viper
	cg   string
}

func NewService(name string, cfgGroup string) *BaseService {

	var v *viper.Viper
	if cfgGroup != "" {
		v = viper.Sub(cfgGroup)
		if v == nil {
			log.Fatalf("No such config group: %s", cfgGroup)
		}
	}

	ccSize := -1
	if v != nil {
		ccSize = v.GetInt("svc.cc-size")
	}

	var cc ContextChannel
	if ccSize > 0 {
		cc = NewContextChannel(ccSize)
	}

	return &BaseService{
		name: name,
		cc:   cc,
		v:    v,
		cg:   cfgGroup,
	}
}

func (s *BaseService) Name() string {
	return s.name
}

func (s *BaseService) CC() ContextChannel {
	return s.cc
}

func (s *BaseService) Cfg() *viper.Viper {
	return s.v
}

func (s *BaseService) OnCfgChange() {

	log.Infof("OnCfgChange in service: %s", s.name)
	if s.cg != "" {
		v := viper.Sub(s.cg)
		if v == nil {
			log.Errorf("Config change, but no such config group: %s in service: %s",
				s.cg, s.name)
		} else {
			s.v = v
		}
	}
}

func (s *BaseService) Start() error {
	log.Infof("Start service '%s'", s.name)
	return nil
}

func (s *BaseService) Stop() error {

	if s.cc != nil {
		s.cc.Close()
	}
	log.Infof("Service '%s' stopped", s.name)
	return nil
}
