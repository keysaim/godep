package ctx

import (
	"context"
	"time"
)

type Context interface {
	Done() <-chan struct{}
	SetDone()
	Error() error
	SetError(err error)
}

type BaseContext struct {
	ctx    context.Context
	cancel context.CancelFunc
	err    error
}

func NewContext(timeout int, pctx context.Context) *BaseContext {

	c := &BaseContext{}
	c.ctx, c.cancel = context.WithTimeout(pctx, time.Duration(timeout)*time.Millisecond)
	return c
}

func (c *BaseContext) Done() <-chan struct{} {
	return c.ctx.Done()
}

func (c *BaseContext) SetDone() {
	c.cancel()
}

func (c *BaseContext) Error() error {
	return c.err
}

func (c *BaseContext) SetError(err error) {
	c.err = err
}

type RContextChannel interface {
	Recv() <-chan Context
}

type WContextChannel interface {
	Send(c Context)
}

type ContextChannel interface {
	RContextChannel
	WContextChannel
	Size() int
	Close()
}

type baseChannel struct {
	size    int
	ctxChan chan Context
}

func NewContextChannel(size int) ContextChannel {

	return &baseChannel{
		size:    size,
		ctxChan: make(chan Context, size),
	}
}

func (cc *baseChannel) Send(c Context) {

	cc.ctxChan <- c
}

func (cc *baseChannel) Recv() <-chan Context {

	return cc.ctxChan
}

func (cc *baseChannel) Size() int {

	return cc.size
}

func (cc *baseChannel) Close() {

	close(cc.ctxChan)
}
