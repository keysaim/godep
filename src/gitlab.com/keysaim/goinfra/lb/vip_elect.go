package lb

import (
	"context"
	"errors"
	"net"
	"time"

	"gitlab.com/keysaim/goinfra/log"
)

type VipElect struct {
	ctx      context.Context
	cancel   context.CancelFunc
	vipStr   string
	vip      net.IP
	itvl     int
	isLeader bool
}

func NewVipElect(pctx context.Context, vip string, itvl int) *VipElect {

	e := &VipElect{
		vipStr: vip,
		itvl:   itvl,
	}
	e.ctx, e.cancel = context.WithCancel(pctx)
	return e
}

func (e *VipElect) Start() error {

	e.vip = net.ParseIP(e.vipStr)
	if e.vip == nil {
		log.Errorf("Invalid vip %s", e.vipStr)
		return errors.New("Invalid vip")
	}

	go e.mainLoop()
	return nil
}

func (e *VipElect) Stop() {
	e.cancel()
}

func (e *VipElect) IsLeader() bool {
	return e.isLeader
}

func (e *VipElect) mainLoop() {

	log.Infof("Vip elect loop start")
	tick := time.NewTicker(time.Duration(e.itvl) * time.Second)

	for {
		select {
		case <-e.ctx.Done():
			log.Infof("Vip elect loop was canceled")
			return
		case <-tick.C:
			e.onTick()
		}
	}
}

func (e *VipElect) onTick() {

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Errorf("Failed to get the net interface addrs, error: %s", err.Error())
		return
	}

	isLeader := false
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.Equal(e.vip) {
				isLeader = true
				break
			}
		}
	}

	e.isLeader = isLeader
}
