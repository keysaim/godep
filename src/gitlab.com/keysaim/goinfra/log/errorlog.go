package log

import (
	"github.com/Sirupsen/logrus"
)

// FluentHook to send logs via fluentd.
type FluentHook struct {
	Handler *FluentHandler
}

// NewFluentHook creates a new hook to send to fluentd.
func NewFluentHook(host string, port int, tag string) (*FluentHook, error) {
	handler, err := NewFluentHandler(host, port, tag)
	if err != nil {
		return nil, err
	}

	hook := new(FluentHook)
	hook.Handler = handler

	return hook, nil
}

// Fire implements logrus.Hook interface Fire method.
func (f *FluentHook) Fire(entry *logrus.Entry) error {
	msg := f.buildMessage(entry)
	return f.Handler.Fire(&LogEntry{Time: entry.Time, msg: &msg})
}

// Levels implements logrus.Hook interface Levels method.
func (f *FluentHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
		logrus.InfoLevel,
		logrus.DebugLevel,
	}
}

func (f *FluentHook) buildMessage(entry *logrus.Entry) map[string]interface{} {
	data := make(map[string]interface{})

	for k, v := range entry.Data {
		data[k] = v
	}
	data["msg"] = entry.Message
	data["level"] = entry.Level.String()

	return data
}
