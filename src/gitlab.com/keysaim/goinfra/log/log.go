package log

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"golang.org/x/crypto/ssh/terminal"
)

type Fields logrus.Fields
type Level logrus.Level

type MdLogger struct {
	module string
	logger *logrus.Logger
}

var (
	logMap     map[string]*MdLogger
	defLogger  *MdLogger
	logRunTime bool = false

	PanicLevel Level
	FatalLevel Level
	ErrorLevel Level
	WarnLevel  Level
	InfoLevel  Level
	DebugLevel Level
	ErrorKey   string
)

func EnableLogRunTime(enable bool) {
	logRunTime = enable
}

func GetRunTimeInfo(frame int) (file, fName string, line int, ok bool) {
	frame += 1 //skip this function call
	pc, file, line, ok := runtime.Caller(frame)
	if ok {
		fName = runtime.FuncForPC(pc).Name()
		if idx := strings.LastIndex(fName, "/"); idx >= 0 {
			fName = fName[idx+1:]
		}

		if idx := strings.LastIndex(file, "/"); idx >= 0 {
			file = file[idx+1:]
		}

	}
	return
}

func GetRunTimeInfoString(frame int) (string, bool) {
	frame += 1 //skip this function call
	if file, _, line, ok := GetRunTimeInfo(frame); ok {
		//return fmt.Sprintf("%s:%d(%s)", file, line, fName), true
		return fmt.Sprintf("%s:%d", file, line), true
	}
	return "", false
}

func init() {
	logMap = make(map[string]*MdLogger)

	PanicLevel = Level(logrus.PanicLevel)
	FatalLevel = Level(logrus.FatalLevel)
	ErrorLevel = Level(logrus.ErrorLevel)
	WarnLevel = Level(logrus.WarnLevel)
	InfoLevel = Level(logrus.InfoLevel)
	DebugLevel = Level(logrus.DebugLevel)
	ErrorKey = logrus.ErrorKey

	defLogger = new(MdLogger)
	defLogger.Init("default", InfoLevel)
}

const (
	nocolor = 0
	red     = 31
	green   = 32
	yellow  = 33
	blue    = 36
	gray    = 37
)

func checkIfTerminal(w io.Writer) bool {
	switch v := w.(type) {
	case *os.File:
		return terminal.IsTerminal(int(v.Fd()))
	default:
		return false
	}
}

type PlainFormatter struct {
	checked    bool
	isTerminal bool
}

func (f *PlainFormatter) Format(entry *logrus.Entry) ([]byte, error) {

	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	if !f.checked {
		if entry.Logger != nil {
			f.isTerminal = checkIfTerminal(entry.Logger.Out)
		}
		f.checked = true
	}

	//b.WriteString(entry.Time.Format(time.RFC3339Nano))
	b.WriteString(entry.Time.Format("2006/01/02 15:04:05.000"))
	b.WriteString(" [")
	f.printColoredLevel(b, entry)
	b.WriteString("]:")

	ro, ok := entry.Data["runtime"]
	if ok {
		b.WriteByte(' ')
		b.WriteString(ro.(string))
	}

	ms := entry.Data["module"].(string)
	if ms != "default" && ms != "" {
		if !ok {
			b.WriteByte(' ')
		}
		b.WriteByte('(')
		b.WriteString(ms)
		b.WriteByte(')')
	}

	b.WriteByte(' ')
	b.WriteString(entry.Message)
	b.WriteByte('\n')
	return b.Bytes(), nil
}

func (f *PlainFormatter) printColoredLevel(b *bytes.Buffer, entry *logrus.Entry) {

	var levelColor int
	switch entry.Level {
	case logrus.DebugLevel:
		levelColor = gray
	case logrus.WarnLevel:
		levelColor = yellow
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		levelColor = red
	default:
		levelColor = blue
	}

	levelText := strings.ToUpper(entry.Level.String())[0:4]
	if f.isTerminal {
		fmt.Fprintf(b, "\x1b[%dm%s\x1b[0m", levelColor, levelText)
	} else {
		fmt.Fprintf(b, "%s", levelText)
	}
}

func (ml *MdLogger) Init(module string, level Level) {
	ml.module = module
	logger := logrus.New()
	ml.logger = logger
	ml.SetLevel(level)
	logger.Formatter = &PlainFormatter{}
}

func (ml *MdLogger) SetOutput(out io.Writer) {
	ml.logger.Out = out
}

func (ml *MdLogger) AddHook(hook logrus.Hook) {
	ml.logger.Hooks.Add(hook)
}

func (ml *MdLogger) SetLevel(level Level) {
	ml.logger.Level = logrus.Level(level)
}

func (ml *MdLogger) GetLevel() Level {
	return Level(ml.logger.Level)
}

func (ml *MdLogger) WithError(err error) *logrus.Entry {
	return ml.WithField(logrus.ErrorKey, err)
}

func (ml *MdLogger) WithField(key string, value interface{}) *logrus.Entry {
	fields := Fields{key: value}
	return ml.WithFields(fields)
}

func (ml *MdLogger) WithFields(fields Fields) *logrus.Entry {
	fields["module"] = ml.module
	return ml.logger.WithFields(logrus.Fields(fields))
}

func (ml *MdLogger) Debug(args ...interface{}) {
	if ml.GetLevel() < DebugLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Debug(args...)
}

func (ml *MdLogger) Print(args ...interface{}) {
	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Print(args...)
}

func (ml *MdLogger) Info(args ...interface{}) {
	if ml.GetLevel() < InfoLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Info(args...)
}

func (ml *MdLogger) Warn(args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warn(args...)
}

func (ml *MdLogger) Warning(args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warning(args...)
}

func (ml *MdLogger) Error(args ...interface{}) {
	if ml.GetLevel() < ErrorLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Error(args...)
}

func (ml *MdLogger) Panic(args ...interface{}) {
	if ml.GetLevel() < PanicLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Panic(args...)
}

func (ml *MdLogger) Fatal(args ...interface{}) {
	if ml.GetLevel() < FatalLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Fatal(args...)
}

func (ml *MdLogger) Debugf(format string, args ...interface{}) {
	if ml.GetLevel() < DebugLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Debugf(format, args...)
}

func (ml *MdLogger) Printf(format string, args ...interface{}) {
	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Printf(format, args...)
}

func (ml *MdLogger) Infof(format string, args ...interface{}) {
	if ml.GetLevel() < InfoLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Infof(format, args...)
}

func (ml *MdLogger) Warnf(format string, args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warnf(format, args...)
}

func (ml *MdLogger) Warningf(format string, args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warningf(format, args...)
}

func (ml *MdLogger) Errorf(format string, args ...interface{}) {
	if ml.GetLevel() < ErrorLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Errorf(format, args...)
}

func (ml *MdLogger) Panicf(format string, args ...interface{}) {
	if ml.GetLevel() < PanicLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Panicf(format, args...)
}

func (ml *MdLogger) Fatalf(format string, args ...interface{}) {
	if ml.GetLevel() < FatalLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Fatalf(format, args...)
}

func (ml *MdLogger) Debugln(args ...interface{}) {
	if ml.GetLevel() < DebugLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Debugln(args...)
}

func (ml *MdLogger) Println(args ...interface{}) {
	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Println(args...)
}

func (ml *MdLogger) Infoln(args ...interface{}) {
	if ml.GetLevel() < InfoLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Infoln(args...)
}

func (ml *MdLogger) Warnln(args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warnln(args...)
}

func (ml *MdLogger) Warningln(args ...interface{}) {
	if ml.GetLevel() < WarnLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Warningln(args...)
}

func (ml *MdLogger) Errorln(args ...interface{}) {
	if ml.GetLevel() < ErrorLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Errorln(args...)
}

func (ml *MdLogger) Panicln(args ...interface{}) {
	if ml.GetLevel() < PanicLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Panicln(args...)
}

func (ml *MdLogger) Fatalln(args ...interface{}) {
	if ml.GetLevel() < FatalLevel {
		return
	}

	fields := Fields{}
	fields["time"] = time.Now().UTC().String()
	if logRunTime {
		if info, ok := GetRunTimeInfoString(2); ok {
			fields["runtime"] = info
		}
	}
	ml.WithFields(fields).Fatalln(args...)
}

//=================================================exported func=====================
func Register(module string, level Level) *MdLogger {
	ml := new(MdLogger)
	ml.Init(module, level)
	logMap[module] = ml
	return ml
}

func GetLogger(module string) *MdLogger {
	if ml, ok := logMap[module]; ok {
		return ml
	} else {
		return nil
	}
}

func SetOutput(module string, out io.Writer) bool {
	if ml := GetLogger(module); ml != nil {
		ml.SetOutput(out)
		return true
	}

	return false
}

func SetDefOutput(out io.Writer) bool {
	defLogger.SetOutput(out)
	return true
}

func SetLevel(module string, level Level) bool {
	ml := GetLogger(module)
	if ml != nil {
		ml.SetLevel(level)
		return true
	} else {
		logrus.WithFields(logrus.Fields{
			"module": module,
		}).Error("no such module")
		return false
	}
}

func GetLevel(module string) (Level, error) {
	ml := GetLogger(module)
	if ml != nil {
		return ml.GetLevel(), nil
	} else {
		logrus.WithFields(logrus.Fields{
			"module": module,
		}).Error("no such module")
		return InfoLevel, errors.New("no such module")
	}
}

func SetDefLevel(level Level) bool {
	defLogger.SetLevel(level)
	return true
}

func SetDefLevelStr(ls string) {
	l := StrToLevel(ls)
	SetDefLevel(l)
}

func StrToLevel(ls string) Level {

	level := InfoLevel

	switch ls {
	case "debug":
		level = DebugLevel
	case "warn":
		level = WarnLevel
	case "error":
		level = ErrorLevel
	case "fatal":
		level = FatalLevel
	default:
		level = InfoLevel
	}
	return level
}

func AddHook(module string, hook logrus.Hook) bool {
	ml := GetLogger(module)
	if ml != nil {
		ml.AddHook(hook)
		return true
	} else {
		logrus.WithFields(logrus.Fields{
			"module": module,
		}).Error("no such module")
		return false
	}
}

func AddDefHook(hook logrus.Hook) bool {
	defLogger.AddHook(hook)
	return true
}

func Md(module string) *MdLogger {
	if ml := GetLogger(module); ml != nil {
		return ml
	} else {
		return defLogger
	}
}

func WithError(err error) *logrus.Entry {
	return defLogger.WithField(logrus.ErrorKey, err)
}

func WithField(key string, value interface{}) *logrus.Entry {
	return defLogger.WithField(key, value)
}

func WithFields(fields Fields) *logrus.Entry {
	return defLogger.WithFields(fields)
}

func Debug(args ...interface{}) {
	defLogger.Debug(args...)
}

func Print(args ...interface{}) {
	defLogger.Print(args...)
}

func Info(args ...interface{}) {
	defLogger.Info(args...)
}

func Warn(args ...interface{}) {
	defLogger.Warn(args...)
}

func Warning(args ...interface{}) {
	defLogger.Warning(args...)
}

func Error(args ...interface{}) {
	defLogger.Error(args...)
}

func Panic(args ...interface{}) {
	defLogger.Panic(args...)
}

func Fatal(args ...interface{}) {
	defLogger.Fatal(args...)
}

func Debugf(format string, args ...interface{}) {
	defLogger.Debugf(format, args...)
}

func Printf(format string, args ...interface{}) {
	defLogger.Printf(format, args...)
}

func Infof(format string, args ...interface{}) {
	defLogger.Infof(format, args...)
}

func Warnf(format string, args ...interface{}) {
	defLogger.Warnf(format, args...)
}

func Warningf(format string, args ...interface{}) {
	defLogger.Warningf(format, args...)
}

func Errorf(format string, args ...interface{}) {
	defLogger.Errorf(format, args...)
}

func Panicf(format string, args ...interface{}) {
	defLogger.Panicf(format, args...)
}

func Fatalf(format string, args ...interface{}) {
	defLogger.Fatalf(format, args...)
}

func Debugln(args ...interface{}) {
	defLogger.Debugln(args...)
}

func Println(args ...interface{}) {
	defLogger.Println(args...)
}

func Infoln(args ...interface{}) {
	defLogger.Infoln(args...)
}

func Warnln(args ...interface{}) {
	defLogger.Warnln(args...)
}

func Warningln(args ...interface{}) {
	defLogger.Warningln(args...)
}

func Errorln(args ...interface{}) {
	defLogger.Errorln(args...)
}

func Panicln(args ...interface{}) {
	defLogger.Panicln(args...)
}

func Fatalln(args ...interface{}) {
	defLogger.Fatalln(args...)
}
