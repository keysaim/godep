package log

import (
	"github.com/Sirupsen/logrus"
	"github.com/fluent/fluent-logger-golang/fluent"
	"time"
)

type LogEntry struct {
	Time time.Time
	msg  *map[string]interface{}
}

// FluentHandler to send logs via fluentd.
type FluentHandler struct {
	Fluent   *fluent.Fluent
	Tag      string
	SendChan chan *LogEntry
}

func NewFluentHandler(host string, port int, tag string) (*FluentHandler, error) {
	handler := new(FluentHandler)
	err := handler.Init(host, port, tag)
	return handler, err
}

// NewFluentHandler creates a new hook to send to fluentd.
func (f *FluentHandler) Init(host string, port int, tag string) error {
	logrus.WithFields(logrus.Fields{
		"host": host,
		"port": port,
	}).Info("create fluent hook")

	config := fluent.Config{FluentHost: host, FluentPort: port}
	logger, err := fluent.New(config)
	if err != nil {
		return err
	}
	f.Fluent = logger
	f.Tag = tag
	f.SendChan = make(chan *LogEntry, 20)
	f.Start()
	return nil
}

func (f *FluentHandler) Start() {
	go f.serve()
}

func (f *FluentHandler) serve() {
	logrus.Info("FluentHandler start serving...")
	for entry := range f.SendChan {
		f.send(entry)
	}
}

func (f *FluentHandler) send(entry *LogEntry) error {
	if err := f.Fluent.PostWithTime(f.Tag, entry.Time, *(entry.msg)); err != nil {
		logrus.WithError(err).Error("post msg failed")
		return err
	}
	return nil
}

func (f *FluentHandler) Fire(entry *LogEntry) error {
	f.SendChan <- entry
	return nil
}
