package idgen

import (
	"fmt"

	"github.com/bwmarrin/snowflake"
	"gitlab.com/keysaim/goinfra/log"
)

func init() {
	snowflake.Epoch = 1514764800000 // in ms, January 1, 2018 12:00:00 AM
}

type IdGen struct {
	node *snowflake.Node
}

func NewIdGen(nodeId int) (*IdGen, error) {

	n, err := snowflake.NewNode(int64(nodeId))
	if err != nil {
		log.Error("Failed to create snowflake node:", err)
		return nil, err
	}

	return &IdGen{
		node: n,
	}, nil
}

func (g *IdGen) Gen() string {

	id := g.node.Generate()
	idstr := fmt.Sprintf("%s", id)
	return idstr
}

func (g *IdGen) GenInt() int64 {

	id := g.node.Generate()
	return id.Int64()
}
