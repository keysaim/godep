package main

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/relay"
)

var debugLevel string
var srcParam relay.RelayParam
var sikParam relay.RelayParam

var cfgFile string = ""

var rootCmd = &cobra.Command{
	Use:   "Stream Relay",
	Short: "Relay",
	Run: func(cmd *cobra.Command, args []string) {

		run()
	},
}

func init() {
	log.EnableLogRunTime(true)

	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&srcParam.Mode, "src-mode", "", "", "source tcp_active/tcp_passive/udp")
	rootCmd.PersistentFlags().StringVarP(&srcParam.LclPort, "src-lport", "", "", "source local port")
	rootCmd.PersistentFlags().StringVarP(&srcParam.RmtPort, "src-rport", "", "", "source remote port")
	rootCmd.PersistentFlags().StringVarP(&srcParam.RmtIp, "src-rip", "", "", "source remote ip")
	rootCmd.PersistentFlags().StringVarP(&sikParam.Mode, "sik-mode", "", "", "sink tcp_active/tcp_passive/udp")
	rootCmd.PersistentFlags().StringVarP(&sikParam.LclPort, "sik-lport", "", "", "sink local port")
	rootCmd.PersistentFlags().StringVarP(&sikParam.RmtPort, "sik-rport", "", "", "sink remote port")
	rootCmd.PersistentFlags().StringVarP(&sikParam.RmtIp, "sik-rip", "", "", "sink remote ip")
	rootCmd.PersistentFlags().StringVarP(&debugLevel, "debug-level", "d", "info", "The debug level")
}

func initConfig() {

	log.SetDefLevelStr(debugLevel)
	if len(srcParam.Mode) == 0 {
		log.Fatal("The source mode is not set")
	}
	if len(sikParam.Mode) == 0 {
		log.Fatal("The sink mode is not set")
	}
}

func run() {

	pctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sr := relay.NewSingleRelay(pctx, &srcParam, &sikParam)
	err := sr.Start()
	if err != nil {
		log.Fatal("Failed to start single relay: ", err)
	}
	err = <-sr.Done()
	log.Infof("Relay exited with: (%T) %v", err, err)
}

func main() {

	rootCmd.Execute()
}
