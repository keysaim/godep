package buffer

import (
	"testing"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type BufferxTestSuite struct {
	suite.Suite
}

func (ts *BufferxTestSuite) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")
}

func (ts *BufferxTestSuite) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *BufferxTestSuite) TestReadWrite() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	n, err := buf.Write(d)
	assert.Equal(n, len(s))
	assert.Nil(err)
	assert.Equal(buf.String(), s)

	data := make([]byte, 6)
	n, err = buf.Read(data)
	assert.Equal(n, 6)
	assert.Nil(err)
	assert.Equal(buf.String(), "world!")
}

func (ts *BufferxTestSuite) TestWriteAt() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	buf.Write(d)
	n, err := buf.WriteAt([]byte("hello"), 6)
	assert.Equal(n, len("hello"))
	assert.Nil(err)
	assert.Equal(buf.String(), "hello hello!")
}

func (ts *BufferxTestSuite) TestWriteAtEof() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	buf.Write(d)
	ws := "hello hello hello"
	n, err := buf.WriteAt([]byte(ws), 6)
	assert.Equal(n, len(s)-6)
	assert.NotNil(err)
	assert.Equal(buf.String(), "hello hello ")
}

func (ts *BufferxTestSuite) TestReadAt() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	buf.Write(d)

	data := make([]byte, 4)
	n, err := buf.ReadAt(data, 6)
	assert.Equal(n, 4)
	assert.Nil(err)
	assert.Equal(string(data), "worl")

	assert.Equal(buf.String(), s)
}

func (ts *BufferxTestSuite) TestReadAtEof() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	buf.Write(d)

	data := make([]byte, 64)
	n, err := buf.ReadAt(data, 6)
	assert.Equal(n, 6)
	assert.NotNil(err)
	assert.Equal(string(data[:n]), "world!")

	assert.Equal(buf.String(), s)
}

func (ts *BufferxTestSuite) TestLimit() {
	assert := require.New(ts.T())

	buf := NewBufferx(make([]byte, 0, 64))
	s := "hello world!"
	d := []byte(s)
	buf.Write(d)

	size, err := buf.Limit(16)
	assert.Nil(err)
	assert.Equal(size, 16)
	assert.Equal(buf.String(), s)

	buf.Next(6)
	assert.Equal(6, buf.Len())
	n, err := buf.WriteString("hello ")
	assert.Equal(6, n)
	assert.Nil(err)
	assert.Equal(12, buf.Len())
	assert.Equal(size, 16)

	n, err = buf.WriteString("hello ")
	assert.Equal(4, n)
	assert.Equal(16, buf.Len())
	assert.Equal(size, 16)
	assert.Equal(BUF_FULL, err)

	buf.Shrink(1024, false)
	assert.Equal(16, buf.Len())
	assert.Equal(16, buf.Cap())
}

func TestBufferx(t *testing.T) {
	suite.Run(t, new(BufferxTestSuite))
}
