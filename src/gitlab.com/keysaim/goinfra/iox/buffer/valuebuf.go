package buffer

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
)

func ReadBitValue(data []byte, order binary.ByteOrder, pval interface{}, start int, bits int) (int, error) {
	sidx := start / 8
	eidx := (start + bits - 1) / 8
	size := eidx - sidx + 1
	if eidx >= len(data) {
		return 0, errors.New(fmt.Sprintf("Not enough data, data length: %d bytes, bits offset: %d bytes, bits size: %d bytes", len(data), sidx, size))
	}

	vd := data[sidx : eidx+1]
	bd := make([]byte, 8)
	off := 8 - len(vd)
	if off < 0 {
		return 0, errors.New("the bytes are over 8")
	}
	if order == binary.LittleEndian {
		copy(bd, vd)
	} else {
		copy(bd[off:], vd)
	}
	var val uint64
	err := binary.Read(bytes.NewReader(bd), order, &val)
	if err != nil {
		return 0, err
	}
	offset := uint32(start % 8)
	val = val >> offset
	left := 8 - offset
	switch pv := pval.(type) {
	case *int8:
		*pv = int8(val) << left >> left
	case *uint8:
		*pv = uint8(val) << left >> left
	case *int16:
		*pv = int16(val) << left >> left
	case *uint16:
		*pv = uint16(val) << left >> left
	case *int32:
		*pv = int32(val) << left >> left
	case *uint32:
		*pv = uint32(val) << left >> left
	case *int64:
		*pv = int64(val) << left >> left
	case *uint64:
		*pv = uint64(val) << left >> left
	default:
		return 0, errors.New(fmt.Sprintf("unsupported type: %v\n", pv))
	}
	return eidx + 1, nil
}

type ValueBuf struct {
	*Bufferx
}

func NewValueBuf(size int) *ValueBuf {
	return &ValueBuf{
		Bufferx: NewBufferx(make([]byte, 0, size)),
	}
}

func NewValueBufBytes(data []byte) *ValueBuf {
	return &ValueBuf{
		Bufferx: NewBufferx(data),
	}
}

func (buf *ValueBuf) PeekBitValue(order binary.ByteOrder, pval interface{}, start int, bits int) (int, error) {
	return ReadBitValue(buf.Bytes(), order, pval, start, bits)
}

func (buf *ValueBuf) ReadValue(order binary.ByteOrder, data interface{}) error {
	err := binary.Read(buf, order, data)
	if err != nil {
		return err
	}
	return err
}

func (buf *ValueBuf) ReadInt16(order binary.ByteOrder) (int16, error) {
	var val int16
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) ReadInt32(order binary.ByteOrder) (int32, error) {
	var val int32
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) ReadInt64(order binary.ByteOrder) (int64, error) {
	var val int64
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) ReadUint16(order binary.ByteOrder) (uint16, error) {
	var val uint16
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) ReadUint32(order binary.ByteOrder) (uint32, error) {
	var val uint32
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) ReadUint64(order binary.ByteOrder) (uint64, error) {
	var val uint64
	err := buf.ReadValue(order, &val)
	return val, err
}

func (buf *ValueBuf) WriteValue(order binary.ByteOrder, data interface{}) error {
	err := binary.Write(buf, order, data)
	if err != nil {
		return err
	}
	return err
}

func (buf *ValueBuf) WriteValueAt(order binary.ByteOrder, data interface{}, off int64) error {
	tb := NewValueBuf(8)
	err := binary.Write(tb, order, data)
	if err != nil {
		return err
	}
	_, err = buf.WriteAt(tb.Bytes(), off)
	return err
}

func (buf *ValueBuf) WriteInt16(order binary.ByteOrder, val int16) error {
	err := buf.WriteValue(order, val)
	return err
}

func (buf *ValueBuf) WriteInt32(order binary.ByteOrder, val int32) error {
	err := buf.WriteValue(order, val)
	return err
}

func (buf *ValueBuf) WriteInt64(order binary.ByteOrder, val int64) error {
	err := buf.WriteValue(order, val)
	return err
}

func (buf *ValueBuf) WriteUint16(order binary.ByteOrder, val uint16) error {
	err := buf.WriteValue(order, val)
	return err
}

func (buf *ValueBuf) WriteUint32(order binary.ByteOrder, val uint32) error {
	err := buf.WriteValue(order, val)
	return err
}

func (buf *ValueBuf) WriteUint64(order binary.ByteOrder, val uint64) error {
	err := buf.WriteValue(order, val)
	return err
}
