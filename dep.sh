#!/bin/bash

export GOPATH="$HOME/ngo"

function get()
{
    local repo="$1"
    local update="$2"
    local version="$3"
    if [ ! -d "$GOPATH/src/$repo" ]; then
        set -x
        go get --insecure -u -v $repo
        { set +x; } > /dev/null 2>&1
    else
        echo "The repo $repo already exists"
        if [ "$update" == "true" ]; then
            cd $GOPATH/src/$repo
            git pull
        fi
    fi
    if [ "$version" != "" ]; then
        cd $GOPATH/src/$repo
        git checkout "$version"
    fi
}

function getx()
{
    local pwd="$PWD"
    local repo="$1"
    local name="$2"
    local version="$3"
    cd "$GOPATH/src/golang.org/x/"
    if [ ! -d "$name" ]; then
        set -x
        git clone $repo
        { set +x; } > /dev/null 2>&1
    else
        echo "The repo golang.org/x/$name already exists"
    fi
    if [ "$version" != "" ]; then
        cd "$GOPATH/src/golang.org/x/$name"
        git checkout "$version"
    fi
}

mkdir -p $GOPATH/src/golang.org/x/
getx "https://github.com/golang/crypto.git" crypto "8dd112bcdc25174059e45e07517d9fc663123347"
getx "https://github.com/golang/lint.git" lint "5614ed5bae6fb75893070bdc0996a68765fdd275"
getx "https://github.com/golang/net.git" net "16b79f2e4e95ea23b2bf9903c9809ff7b013ce85"
getx "https://github.com/golang/sys.git" sys "30e92a19ae4a77dde818b8c3d41d51e4850cba12"
getx "https://github.com/golang/text.git" text "v0.3.0"
getx "https://github.com/golang/tools.git" tools "f8c04913dfb7b2339a756441456bdbe0af6eb508"
getx "golang.org/x/time" "time" "85acf8d2951cb2a3bde7632f9ff273ef0379bcbd"
#getx "https://go.googlesource.com/tools" tools

get "github.com/fsnotify/fsnotify" "" "v1.4.7"
get "github.com/hashicorp/consul/api" "" "v1.4.3"
get "github.com/hashicorp/hcl" "" "v1.0.0"
get "github.com/labstack/echo" "" "v3.3.9"
get "github.com/labstack/gommon/random" "" "v0.2.8"
get "github.com/magiconair/properties" "" "v1.8.0"
get "github.com/spf13/cobra" "" "v0.0.3"
get "github.com/spf13/viper" "" "v1.3.1"
get "github.com/spf13/jwalterweatherman" "" "v1.1.0"
get "gitlab.alibaba-inc.com/baoping.hbp/goinfra/httpx" "true"
get "gitlab.com/keysaim/goinfra/httpx"
get "github.com/Sirupsen/logrus" "" "v1.3.0"
get "github.com/mattn/go-colorable" "" "v0.1.1"
get "github.com/mattn/go-isatty" "" "v0.0.6"
get "github.com/dgrijalva/jwt-go" "" "v3.2.0"
get "github.com/fluent/fluent-logger-golang/fluent" "" "v1.4.0"
get "github.com/go-resty/resty" "" "v1.12.0"
get "github.com/tinylib/msgp/msgp" "" "v1.1.0"
get "github.com/serialx/hashring" "" "49a4782e9908fe098c907022a1bd7519c79803d6"
get "github.com/go-redis/redis" "" "v6.15.2"
get "github.com/aliyun/aliyun-log-go-sdk" "" "0.1.0"
get "github.com/aliyun/aliyun-oss-go-sdk/oss" "" "1.9.4"
get "github.com/gogo/protobuf/proto" "" "v1.2.1"
get "github.com/patrickmn/go-cache" "" "v2.1.0"
get "github.com/mitchellh/mapstructure" "" "v1.1.2"
get "github.com/pelletier/go-toml" "" "v1.2.0"
get "github.com/philhofer/fwd" "" "v1.0.0"
get "github.com/spf13/afero" "" "v1.2.1"
get "github.com/spf13/cast" "" "v1.3.0"
get "github.com/spf13/pflag" "" "v1.0.3"
get "github.com/valyala/fasttemplate" "" "v1.0.0"
get "gopkg.in/yaml.v2" "" "v2.2.2"
get "github.com/discoviking/fsm" "" "f4a273feecca48572c13e9c232e8d4cbeb889fec"
get "github.com/clbanning/mxj" "" "v1.8.4"
get "github.com/satori/go.uuid" "" "b2ce2384e17bbe0c6d34077efa39dbab3e09123b"
get "github.com/stretchr/testify/require" "" "v1.3.0"
get "github.com/tealeg/xlsx" "" "v1.0.3"
get "github.com/davecgh/go-spew/spew" "" "v1.1.1"
get "github.com/go-sql-driver/mysql" "" "v1.4.1"
get "github.com/jmoiron/sqlx" "" "v1.2.0"
get "github.com/tidwall/redcon" "" "v1.0.0"
get "github.com/go-playground/universal-translator" "" "v0.16.0"
get "github.com/go-playground/locales" "" "v0.12.1"
get "gopkg.in/go-playground/validator.v9" "" "v9.29.0"
get "github.com/leodido/go-urn" "" "v1.1.0"
get "github.com/miekg/dns" "" "v1.1.29"
get "github.com/oschwald/geoip2-golang" "" "v1.3.0"
get "github.com/gobwas/glob" "" "v0.2.3"
get "github.com/bwmarrin/snowflake" "" "v0.3.0"
get "github.com/gorilla/websocket" "" "v1.4.1"
get "github.com/jinzhu/inflection" "" "v1.0.0"
